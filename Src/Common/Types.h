#pragma once

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef float f32;
typedef double f64;

typedef size_t umm;
typedef wchar_t wchar;
typedef s32 b32;

#define Kilobytes(N) (1024ULL * (N))
#define Megabytes(N) (1024ULL * Kilobytes(N))
#define Gigabytes(N) (1024ULL * Megabytes(N))
#define Terabytes(N) (1024ULL * Gigabytes(N))

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))
#define PackRGB(R, G, B) (((R) << 0) | ((G) << 8) | ((B) << 16))
#define PackRGBA(R, G, B, A) (PackRGB((R), (G), (B)) | ((A) << 24))

#if defined(_MSC_VER)

#define CHK_BEGIN_PACKED_STRUCT(Name) \
    _Pragma("pack(push)") typedef struct Name

#define CHK_END_PACKED_STRUCT(Name) \
    Name;                           \
    _Pragma("pack(pop)")

#else

#define CHK_BEGIN_PACKED_STRUCT(Name) \
    typedef struct Name

#define CHK_END_PACKED_STRUCT(Name) \
                                    \
    __attribute__((__packed__)) Name;

#endif
