#pragma once

#include "Types.h"

typedef struct chk_bitmap
{
    s32 W, H;
    s32 BPP;
    s32 Pitch;
    umm MemorySize;
    u8* Memory;
} chk_bitmap;
