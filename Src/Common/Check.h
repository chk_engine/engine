#pragma once

/*
    Check(Exp) -> void

    Checks if the expression is true or not, aborting the program if false.
    This works even on .DLL/.so
*/

#include "Types.h"

#define CHK_CHECK_FUNC(Name) void Name(void)
typedef CHK_CHECK_FUNC(chk_check_func);

typedef struct chk_debug_checker
{
    chk_check_func* Check;
} chk_debug_checker;

#if defined(CHK_NOPLATFORM)
// Rely on the platform implementation
#define CHK_CheckImp g_Engine->m_DebugChecker->Check
#else
// Implement those ourselves
CHK_CHECK_FUNC(CHKP_CheckImp);

#define CHK_CheckImp CHKP_CheckImp
#endif

#define CHECK(Exp)                                                                                             \
    do {                                                                                                       \
        if (!(Exp))                                                                                            \
        {                                                                                                      \
            CHK_LogFormatImp("Check", "Checking expression '%s' failed!", __FILE__, __FUNC__, __LINE__, #Exp); \
            CHK_CheckImp();                                                                                    \
        }                                                                                                      \
    } while (0)
