#pragma once

#include "Types.h"

typedef struct chk_gamepad_button
{
    u32 Id;
    b32 State, Pressed, Released, Last;
} chk_gamepad_button;

typedef struct chk_gamepad_axis
{
    u32 Id;
    f32 State;
} chk_gamepad_axis;

typedef struct chk_gamepad_input
{
    b32 Loaded, Active;
    u32 PlayerID;

    chk_gamepad_button LeftU, LeftD, LeftL, LeftR;
    chk_gamepad_button RightU, RightD, RightL, RightR;

    chk_gamepad_button Start, Select, Home;

    chk_gamepad_button L1, L3;
    chk_gamepad_axis L2;

    chk_gamepad_button R1, R3;
    chk_gamepad_axis R2;

    chk_gamepad_axis LStickX, LStickY;
    chk_gamepad_axis RStickX, RStickY;
} chk_gamepad_input;
