#pragma once

#include "Engine.h"
#include "Threading.h"
#include "Types.h"

#define CHK_PROCESS_FRAME(Name) void Name(chk_thread_context* Thread, chk_app_engine* Engine)
#define CHK_PROCESS_AUDIO(Name) void Name(chk_thread_context* Thread, chk_app_engine* Engine)

typedef CHK_PROCESS_FRAME(chk_process_frame_func);
typedef CHK_PROCESS_FRAME(chk_process_audio_func);

static chk_app_engine* g_Engine = NULL;

#ifdef _WIN32
#   define MODULE_API __declspec(dllexport)
#else
#   define MODULE_API
#endif
