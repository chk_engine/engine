#pragma once

#include "Types.h"

typedef struct chk_debug_file
{
    u8* Memory;
    umm MemorySize;
} chk_debug_file;

#define CHK_DEBUG_LOAD_FILE(Name) b32 Name(const char* Filename, chk_debug_file* FileOut)
#define CHK_DEBUG_WRITE_FILE(Name) b32 Name(const char* Filename, chk_debug_file* FileIn)
#define CHK_DEBUG_FREE_FILE(Name) b32 Name(chk_debug_file* FileIn)

typedef CHK_DEBUG_LOAD_FILE(chk_debug_load_file_func);
typedef CHK_DEBUG_WRITE_FILE(chk_debug_write_file_func);
typedef CHK_DEBUG_FREE_FILE(chk_debug_free_file_func);

typedef struct chk_debug_io
{
    chk_debug_load_file_func* LoadFile;
    chk_debug_write_file_func* WriteFile;
    chk_debug_free_file_func* FreeFile;
} chk_debug_io;
