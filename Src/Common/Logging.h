#pragma once

#include "Types.h"

/*
    Log(Sender, Msg) -> void

    Prints the message in a debug-like format.
    Usually in the form:
        [Sender]: File(Line) :: Func => Msg

    LogFormat(Sender, Msg, ...) -> void

    Same as Log(...) but formats the message in a printf like fashion.
    For ease of implementation, it only requires supporting %s %d and %f as a minimum.
*/

#define CHK_LOG_FUNC(Name) void Name(const char* Sender, const char* Msg, const char* File, const char* Func, umm Line)
typedef CHK_LOG_FUNC(chk_log_func);

#define CHK_LOGFORMAT_FUNC(Name) void Name(const char* Sender, const char* Msg, const char* File, const char* Func, umm Line, ...)
typedef CHK_LOGFORMAT_FUNC(chk_logformat_func);

typedef struct chk_debug_logger
{
    chk_log_func* Log;
    chk_logformat_func* LogFormat;
} chk_debug_logger;

#if defined(CHK_NOPLATFORM)
// Rely on the platform implementation
#define CHKP_LogImp(Sender, Msg, File, Func, Line) g_Engine->m_DebugLogger->Log(Sender, Msg, File, Func, Line)
#define CHKP_LogFormatImp(Sender, Msg, File, Func, Line, ...) g_Engine->m_DebugLogger->LogFormat(Sender, Msg, File, Func, Line, __VA_ARGS__)
#else
// Implement those ourselves
CHK_LOG_FUNC(CHKP_LogImp);
CHK_LOGFORMAT_FUNC(CHKP_LogFormatImp);
#endif

// Common logging functions
#define LOG(Sender, Msg) CHKP_LogImp((Sender), (Msg), __FILE__, __func__, __LINE__)
#define LOG_INFO(Msg) CHKP_LogImp("Info", (Msg), __FILE__, __func__, __LINE__)
#define LOG_WARNING(Msg) CHKP_LogImp("Warning", (Msg), __FILE__, __func__, __LINE__)
#define LOG_ERROR(Msg) CHKP_LogImp("Error", (Msg), __FILE__, __func__, __LINE__)

#define LOGF(Sender, Msg, ...) CHKP_LogFormatImp((Sender), (Msg), __FILE__, __func__, __LINE__, __VA_ARGS__)
#define LOGF_INFO(Msg, ...) CHKP_LogFormatImp("Info", (Msg), __FILE__, __func__, __LINE__, __VA_ARGS__)
#define LOGF_WARNING(Msg, ...) CHKP_LogFormatImp("Warning", (Msg), __FILE__, __func__, __LINE__, __VA_ARGS__)
#define LOGF_ERROR(Msg, ...) CHKP_LogFormatImp("Error", (Msg), __FILE__, __func__, __LINE__, __VA_ARGS__)
