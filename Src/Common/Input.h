#pragma once

#include "Gamepad.h"
#include "Types.h"

typedef struct chk_app_input
{
    chk_gamepad_input Gamepads[4];
    umm KeyboardIndex;
} chk_app_input;
