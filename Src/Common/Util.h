#pragma once

#include "Types.h"

void Swap_Implementation(void* p1, void* p2, void* tmp, umm pSize);
#define Swap(A, B) Swap_Implementation(&(A), &(B), (u8[(sizeof(A) == sizeof(B)) ? (ptrdiff_t)sizeof(A) : -1]){0}, sizeof(A))
