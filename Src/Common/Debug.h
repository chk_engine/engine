#pragma once

#include "Check.h"
#include "Logging.h"
#include "Types.h"

#define Assert(Exp)                                                                         \
    do {                                                                                    \
        if (!(Exp))                                                                         \
        {                                                                                   \
            CHK_LogFormatImp("Assertion '%s' failed!", #Exp, __FILE__, __func__, __LINE__); \
            CHK_CheckImp();                                                                 \
        }                                                                                   \
    } while (0)
