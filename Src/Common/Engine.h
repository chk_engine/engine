#pragma once

#include "Audio.h"
#include "Check.h"
#include "DebugIO.h"
#include "Logging.h"
#include "Memory.h"
#include "Threading.h"
#include "Types.h"
#include "Window.h"

typedef struct chk_app_config
{
    s32 XOffset, YOffset;
    b32 Inverted;

    u32 CurrentGamepad;
    u32 Frequency;
    f32 Volume;
} chk_app_config;

typedef struct chk_app_engine
{
    chk_app_window* Window;
    chk_app_audio_output* Audio;
    chk_app_memory* Memory;
    chk_app_config* Config;
    chk_app_input* Input;

    chk_debug_io* m_DebugIO;
    chk_debug_logger* m_DebugLogger;
    chk_debug_checker* m_DebugChecker;
} chk_app_engine;
