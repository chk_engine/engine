#pragma once

#include "Bitmap.h"
#include "Input.h"
#include "Types.h"

struct chk_app_window;
#define CHK_WINDOW_CALLBACK(Name) void Name(struct chk_app_window* Window, void* UserPtr)
typedef CHK_WINDOW_CALLBACK(chk_app_window_callback);

typedef struct chk_app_window
{
    s32 X, Y;
    s32 W, H;

    // State
    b32 IsRunning;
    b32 IsFullscreen;
    b32 IsMinimized;
    b32 IsMaximized;
    b32 IsFocused;

    f32 DeltaTime;
    f32 FramesPerSecond;

    // Stuff
    chk_bitmap Bitmap;
    chk_app_input* Input;

    // Callbacks
    chk_app_window_callback* m_CB_OnSize;
    chk_app_window_callback* m_CB_OnDraw;
    chk_app_window_callback* m_CB_OnMove;
    chk_app_window_callback* m_CB_OnQuit;
    chk_app_window_callback* m_CB_OnFullscreen;

    // UserPointers
    void* m_UP_OnSize;
    void* m_UP_OnDraw;
    void* m_UP_OnMove;
    void* m_UP_OnQuit;
    void* m_UP_OnFullscreen;
} chk_app_window;
