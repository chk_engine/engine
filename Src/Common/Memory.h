#pragma once

#include "Types.h"

typedef struct chk_app_memory
{
    b32 Initialized;

    void* Main;
    void* Temp;

    umm MainSize;
    umm TempSize;
} chk_app_memory;
