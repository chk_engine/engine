#include "Util.h"

#include <string.h>

void Swap_Implementation(void* p1, void* p2, void* tmp, umm pSize)
{
    memcpy(tmp, p1, pSize);
    memcpy(p1, p2, pSize);
    memcpy(p2, tmp, pSize);
}
