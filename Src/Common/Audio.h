#pragma once

#include "Types.h"

typedef struct chk_app_audio_output
{
    u32 NumberOfChannels;
    u32 BitsPerChannel;
    u32 BytesPerSample;
    u32 SamplesPerSecond;
    u32 BufferSize;
    u8* Buffer;

    b32 IsPlaying;
    u32 RunningSampleIndex;
    u32 LatencySampleCount;
} chk_app_audio_output;
