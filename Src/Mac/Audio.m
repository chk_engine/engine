#include "Audio.h"

OSStatus CoreAudioRenderCallback(void* InRefCon,
                                 AudioUnitRenderActionFlags* IOActionFlags,
                                 const AudioTimeStamp* InTimeStamp,
                                 u32 InBusNumber,
                                 u32 InNumberFrames,
                                 AudioBufferList* IOData)
{
    (void)IOActionFlags;
    (void)InTimeStamp;
    (void)InBusNumber;

    chk_macos_app_audio_output* SoundOutput = (chk_macos_app_audio_output*)InRefCon;

    u32 BytesToOutput = InNumberFrames * SoundOutput->Base.BytesPerSample;
    u32 Region1Size = BytesToOutput;
    u32 Region2Size = 0;

    if (SoundOutput->Base.PlayCursor + BytesToOutput > SoundOutput->Base.BufferSize)
    {
        Region1Size = SoundOutput->Base.BufferSize - SoundOutput->Base.PlayCursor;
        Region2Size = BytesToOutput - Region1Size;
    }

    u8* Channel = (uint8*)IOData->mBuffers[0].mData;

    memcpy(Channel, (u8*)SoundOutput->Base.Buffer + SoundOutput->Base.PlayCursor, Region1Size);
    memcpy(&Channel[Region1Size], SoundOutput->Base.Buffer, Region2Size);

    SoundOutput->Base.PlayCursor = (SoundOutput->Base.PlayCursor + BytesToOutput) % SoundOutput->Base.BufferSize;

    return (noErr);
}

b32 MacOS_Audio_Setup(chk_macos_app_audio_output* SoundOutput)
{
    SoundOutput->Base.SamplesPerSecond = 48000;
    s32 AudioFrameSize = sizeof(s16) * 2;
    s32 NumberOfSeconds = 2;

    SoundOutput->Base.BytesPerSample = AudioFrameSize;
    s32 AudioLength = SoundOutput->Base.SamplesPerSecond * NumberOfSeconds;
    SoundOutput->Base.BufferSize = (umm)AudioLength * (umm)AudioFrameSize;
    SoundOutput->Base.Buffer = calloc(AudioLength, AudioFrameSize);
    SoundOutput->Base.PlayCursor = 0;

    AudioComponentInstance AudioUnit = {0};
    SoundOutput->Instance = AudioUnit;

    AudioComponentDescription Acd = {0};
    Acd.componentType = kAudioUnitType_Output;
    Acd.componentSubType = kAudioUnitSubType_DefaultOutput;
    Acd.componentManufacturer = kAudioUnitManufacturer_Apple;
    Acd.componentFlags = 0;
    Acd.componentFlagsMask = 0;

    AudioComponent OutputComponent = AudioComponentFindNext(NULL, &Acd);
    OSStatus Status = AudioComponentInstanceNew(OutputComponent, &SoundOutput->Instance);

    if (Status != noErr)
    {
        NSLog(@"There was an error creating the AudioComponent");
        return (false);
    }

    AudioStreamBasicDescription AudioDescriptor;
    AudioDescriptor.mSampleRate = SoundOutput->Base.SamplesPerSecond;
    AudioDescriptor.mFormatID = kAudioFormatLinearPCM;
    AudioDescriptor.mFormatFlags = kAudioFormatFlagIsSignedInteger |
                                   kAudioFormatFlagIsPacked;
    AudioDescriptor.mFramesPerPacket = 1;
    AudioDescriptor.mChannelsPerFrame = 2;
    AudioDescriptor.mBitsPerChannel = sizeof(s16) * 8;
    AudioDescriptor.mBytesPerFrame = SoundOutput->Base.BytesPerSample;
    AudioDescriptor.mBytesPerPacket = SoundOutput->Base.BytesPerSample;

    Status = AudioUnitSetProperty(SoundOutput->Instance,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  0,
                                  &AudioDescriptor,
                                  sizeof(AudioDescriptor));

    if (Status != noErr)
    {
        NSLog(@"There was an error setting up the AudioComponentInstance");
        return (false);
    }

    AURenderCallbackStruct RenderCallback;
    RenderCallback.inputProcRefCon = (void*)SoundOutput;
    RenderCallback.inputProc = CoreAudioRenderCallback;

    Status = AudioUnitSetProperty(SoundOutput->Instance,
                                  kAudioUnitProperty_SetRenderCallback,
                                  kAudioUnitScope_Global,
                                  0,
                                  &RenderCallback,
                                  sizeof(RenderCallback));

    if (Status != noErr)
    {
        NSLog(@"There was an error setting up the AudioComponentInstance");
        return (false);
    }

    AudioUnitInitialize(SoundOutput->Instance);
    AudioOutputUnitStart(SoundOutput->Instance);

    return (true);
}

// Debug wave rendering
void MacOS_Audio_WriteSquareWave(chk_macos_app_audio_output* Audio, u32 Frequency, f32 Volume)
{
    u32 Period = Audio->Base.SamplesPerSecond / Frequency;
    u32 HalfPeriod = Period / 2;

    u32 LatencySampleCount = Audio->Base.SamplesPerSecond / 15;
    u32 TargetQueueBytes = LatencySampleCount * Audio->Base.BitsPerChannel;

    u32 TargetCursor = ((Audio->Base.PlayCursor + TargetQueueBytes) % Audio->Base.BufferSize);

    u32 ByteToLock = (Audio->Base.RunningSampleIndex * Audio->Base.BitsPerChannel) % Audio->Base.BufferSize;
    u32 BytesToWrite;

    if (ByteToLock == TargetCursor)
    {
        BytesToWrite = Audio->Base.BufferSize;
    } else if (ByteToLock > TargetCursor) {
        BytesToWrite = (Audio->Base.BufferSize - ByteToLock);
        BytesToWrite += TargetCursor;
    } else {
        BytesToWrite = TargetCursor - ByteToLock;
    }

    void* Region1 = (uint8*)Audio->Base.Buffer + ByteToLock;
    uint32 Region1Size = BytesToWrite;

    if (Region1Size + ByteToLock > Audio->Base.BufferSize)
    {
        Region1Size = Audio->Base.BufferSize - ByteToLock;
    }

    void* Region2 = Audio->Base.Buffer;
    u32 Region2Size = BytesToWrite - Region1Size;

    u32 Region1SampleCount = Region1Size / Audio->Base.BytesPerSample;
    s16* SampleOut = (s16*)Region1;

    for (u32 SampleIndex = 0; SampleIndex < Region1SampleCount; ++SampleIndex)
    {
        s16 SampleValue = ((Audio->Base.RunningSampleIndex++ / HalfPeriod) % 2) ? 5000 : -5000;
        s16 ScaledSampleValue = (s16)(Volume * (f32)SampleValue);
        *SampleOut++ = ScaledSampleValue;
        *SampleOut++ = ScaledSampleValue;
    }

    u32 Region2SampleCount = Region2Size / Audio->Base.BytesPerSample;
    SampleOut = (s16*)Region2;

    for (u32 SampleIndex = 0; SampleIndex < Region2SampleCount; ++SampleIndex)
    {
        s16 SampleValue = ((Audio->Base.RunningSampleIndex++ / HalfPeriod) % 2) ? 5000 : -5000;
        s16 ScaledSampleValue = (s16)(Volume * (f32)SampleValue);
        *SampleOut++ = ScaledSampleValue;
        *SampleOut++ = ScaledSampleValue;
    }
}
