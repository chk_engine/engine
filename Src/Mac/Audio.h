#pragma once

#include "../Common/Audio.h"
#import <AudioToolbox/AudioToolbox.h>

typedef struct chk_macos_app_audio_output
{
    chk_app_audio_output Base;
    AudioComponentInstance Instance;
} chk_macos_app_audio_output;

b32 MacOS_Audio_Setup(chk_macos_app_audio_output* SoundOutput);

void MacOS_Audio_WriteSquareWave(chk_macos_app_audio_output* Audio, u32 Frequency, f32 Volume);
