#include "DebugIO.h"
#include "Path.h"

#include <stdio.h>
#include <stdlib.h>

CHK_DEBUG_LOAD_FILE(Debug_LoadFile)
{
    chk_macos_bundle_info BundleInfo = {0};
    MacOS_Bundle_Setup(&BundleInfo);

    char AbsFilename[CHK_MACOS_MAX_FILENAME_SIZE] = {0};
    MacOS_Bundle_PathRelativeToData(&BundleInfo, Filename, StringLength(Filename), AbsFilename, ArrayCount(AbsFilename));

    FILE* FileHandle = fopen(AbsFilename, "rb");
    if (!FileHandle)
    {
        return (false);
    }

    fseek(FileHandle, 0, SEEK_END);
    u64 FileSize = ftell(FileHandle);
    if (!FileSize)
    {
        fclose(FileHandle);
        return (false);
    }

    rewind(FileHandle);
    FileOut->Memory = malloc(FileSize);
    if (!FileOut->Memory)
    {
        fclose(FileHandle);
        return (false);
    }

    u64 BytesRead = fread(FileOut->Memory, 1, FileSize, FileHandle);
    if (BytesRead != FileSize)
    {
        Debug_FreeFile(FileOut);
        fclose(FileHandle);
        return (false);
    }
    fclose(FileHandle);

    FileOut->MemorySize = FileSize;
    return (true);
}

CHK_DEBUG_WRITE_FILE(Debug_WriteFile)
{
    chk_macos_bundle_info BundleInfo = {0};
    MacOS_Bundle_Setup(&BundleInfo);

    char AbsFilename[CHK_MACOS_MAX_FILENAME_SIZE] = {0};
    MacOS_Bundle_PathRelativeToData(&BundleInfo, Filename, StringLength(Filename), AbsFilename, ArrayCount(AbsFilename));

    FILE* FileHandle = fopen(AbsFilename, "wb");
    if (!FileHandle) { return (false); }

    u64 BytesWritten = fwrite(FileIn->Memory, 1, FileIn->MemorySize, FileHandle);
    if (BytesWritten != FileIn->MemorySize)
    {
        fclose(FileHandle);
        return (false);
    }
    fclose(FileHandle);

    return (true);
}

CHK_DEBUG_FREE_FILE(Debug_FreeFile)
{
    if (FileIn && FileIn->Memory)
    {
        free(FileIn->Memory);
        FileIn->Memory = NULL;
        FileIn->MemorySize = 0;
        return (true);
    }
    return (false);
}
