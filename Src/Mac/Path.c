#include "Path.h"
#include <mach-o/dyld.h>

// String
b32 CatStrings(const char* StrA, umm CountA, const char* StrB, umm CountB, char* StrDst, umm CountDst)
{
    b32 CopiedAll = false;

    umm Index, Written = 0;
    for (Index = 0; Index < CountA && Written < CountDst; ++Index, ++Written) { *StrDst++ = *StrA++; }
    for (Index = 0; Index < CountB && Written < CountDst; ++Index, ++Written) { *StrDst++ = *StrB++; }
    if (Written == CountDst)
    {
        --StrDst;
        CopiedAll = false;
    }
    *StrDst++ = 0;

    return (CopiedAll);
}

umm StringLength(const char* Str)
{
    umm Count = 0;

    while (*Str++)
    {
        ++Count;
    }

    return (Count);
}

b32 MacOS_Bundle_Setup(chk_macos_bundle_info* BundleInfo)
{
    if (!BundleInfo) { return (false); }
    BundleInfo->IsValid = true;

    // Get the path to the executable in the bundle
    u32 FileSize = ArrayCount(BundleInfo->Filename);
    if (_NSGetExecutablePath(BundleInfo->Filename, &FileSize) != 0)
    {
        BundleInfo->IsValid = false;
        return (false);
    }

    // Go back two folders in the path to find the Contents folder
    BundleInfo->LastSlash = BundleInfo->SecondLastSlash = BundleInfo->Filename;
    for (char* Scan = BundleInfo->Filename; *Scan; Scan++)
    {
        if (*Scan == '/')
        {
            BundleInfo->SecondLastSlash = BundleInfo->LastSlash;
            BundleInfo->LastSlash = Scan + 1;
        }
    }

    // Construct the path to Contents
    BundleInfo->FilenameCount = (BundleInfo->LastSlash - BundleInfo->Filename);
    BundleInfo->ContentsCount = (BundleInfo->SecondLastSlash - BundleInfo->Filename);

    const char DataPath[] = "Resources/Data/";
    BundleInfo->DataCount = BundleInfo->ContentsCount + ArrayCount(DataPath) - 1;
    if (!MacOS_Bundle_PathRelativeToContents(BundleInfo, DataPath,
                                             ArrayCount(DataPath), BundleInfo->Data,
                                             ArrayCount(BundleInfo->Data)))
    {
        BundleInfo->IsValid = false;
        return (false);
    }

    return (true);
}

b32 MacOS_Bundle_PathRelativeToContents(chk_macos_bundle_info* BundleInfo, const char* Path, umm PathCount, char* Dst, umm DstCount)
{
    if (!BundleInfo || !BundleInfo->IsValid) { return (false); }
    CatStrings(BundleInfo->Filename, BundleInfo->ContentsCount, Path, PathCount, Dst, DstCount);
    return (true);
}

b32 MacOS_Bundle_PathRelativeToExecutable(chk_macos_bundle_info* BundleInfo, const char* Path, umm PathCount, char* Dst, umm DstCount)
{
    if (!BundleInfo || !BundleInfo->IsValid) { return (false); }
    CatStrings(BundleInfo->Filename, BundleInfo->FilenameCount, Path, PathCount, Dst, DstCount);
    return (true);
}

b32 MacOS_Bundle_PathRelativeToData(chk_macos_bundle_info* BundleInfo, const char* Path, umm PathCount, char* Dst, umm DstCount)
{
    if (!BundleInfo || !BundleInfo->IsValid) { return (false); }
    CatStrings(BundleInfo->Filename, BundleInfo->DataCount, Path, PathCount, Dst, DstCount);
    return (true);
}
