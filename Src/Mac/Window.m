#include "Window.h"
#include "Gamepad.h"

b32 MacOS_Window_ResizeBitmap(chk_macos_app_window* Window, s32 W, s32 H)
{
    if (!Window) { return (false); }
    chk_bitmap* Bmp = &Window->Base.Bitmap;

    if (Bmp->Memory)
    {
        free(Bmp->Memory);
        Bmp->Memory = NULL;
        Bmp->MemorySize = 0;
    }

    Bmp->W = W;
    Bmp->H = H;
    Bmp->BPP = 4;
    Bmp->Pitch = Bmp->W * Bmp->BPP;
    Bmp->MemorySize = Bmp->Pitch * Bmp->H;
    Bmp->Memory = malloc(Bmp->MemorySize);

    if (!Bmp->Memory)
    {
        Window->Base.Bitmap = (chk_bitmap){0};
        return (false);
    }

    return (true);
}

b32 MacOS_Window_RenderBitmap(chk_macos_app_window* Window)
{
    if (!Window) { return (false); }

    @autoreleasepool
    {
        chk_bitmap* Bmp = &Window->Base.Bitmap;

        NSBitmapImageRep* Bitmap = [[[NSBitmapImageRep alloc]
            initWithBitmapDataPlanes:&Bmp->Memory
                          pixelsWide:Bmp->W
                          pixelsHigh:Bmp->H
                       bitsPerSample:8
                     samplesPerPixel:Bmp->BPP
                            hasAlpha:YES
                            isPlanar:NO
                      colorSpaceName:NSDeviceRGBColorSpace
                        bitmapFormat:NSBitmapFormatThirtyTwoBitLittleEndian
                         bytesPerRow:Bmp->Pitch
                        bitsPerPixel:(Bmp->BPP * 8)] autorelease];

        NSSize ImageSize = NSMakeSize(Bmp->W, Bmp->H);
        NSImage* Image = [[[NSImage alloc] initWithSize:ImageSize] autorelease];
        [Image addRepresentation:Bitmap];
        Window->Internal.contentView.layer.contents = Image;
    }

    return (true);
}

b32 MacOS_Window_Setup(chk_macos_app_window* Window, const char* Caption)
{
    if (!Window) { return (false); }
    Window->Base.IsRunning = true;

    NSRect ScreenRect = [[NSScreen mainScreen] frame];
    NSRect WindowRect = NSMakeRect(0.5f * (ScreenRect.size.width - Window->Base.W),
                                   0.5f * (ScreenRect.size.height - Window->Base.H),
                                   Window->Base.W, Window->Base.H);

    // Window Creation
    Window->Internal = [[NSWindow alloc] initWithContentRect:WindowRect
                                                   styleMask:(NSWindowStyleMaskTitled |
                                                              NSWindowStyleMaskClosable |
                                                              NSWindowStyleMaskMiniaturizable |
                                                              NSWindowStyleMaskResizable)
                                                     backing:NSBackingStoreBuffered
                                                       defer:YES];
    [Window->Internal setBackgroundColor:NSColor.blackColor];
    [Window->Internal setTitle:@(Caption)];
    [Window->Internal makeKeyAndOrderFront:nil];
    [Window->Internal orderFrontRegardless];
    Window->Internal.contentView.wantsLayer = YES;

    // Window Delegate
    chk_macos_app_window_delegate* Delegate = [[chk_macos_app_window_delegate alloc] initWithWindow:Window];
    [Window->Internal setDelegate:Delegate];

    s32 Width = (s32)Window->Internal.contentView.bounds.size.width;
    s32 Height = (s32)Window->Internal.contentView.bounds.size.height;
    MacOS_Window_ResizeBitmap(Window, Width, Height);
    MacOS_Window_RenderBitmap(Window);

    // Timing
    mach_timebase_info(&Window->TimeBase);
    Window->LastCounter = mach_absolute_time();

    return (Window->Base.IsRunning);
}

b32 MacOS_Window_Destroy(chk_macos_app_window* Window)
{
    if (!Window) { return (false); }

    [Window->Delegate release];
    [Window->Internal release];

    return (true);
}

b32 MacOS_Window_BeginFrame(chk_macos_app_window* Window)
{
    if (!Window) { return (false); }

    return (Window->Base.IsRunning);
}

b32 MacOS_Window_EndFrame(chk_macos_app_window* Window)
{
    if (!Window) { return (false); }

    if (Window->Base.m_CB_OnDraw) { Window->Base.m_CB_OnDraw(&Window->Base, Window->Base.m_UP_OnDraw); }
    MacOS_Window_RenderBitmap(Window);

    // Calculate time delta
    u64 EndOfFrameTime = mach_absolute_time();
    u64 TimeUnitsPerFrame = EndOfFrameTime - Window->LastCounter;

    u64 NanosecondsPerFrame = TimeUnitsPerFrame * (Window->TimeBase.numer / Window->TimeBase.denom);
    f32 SecondsPerFrame = (f32)((f64)NanosecondsPerFrame * 1.0E-9);
    Window->Base.FramesPerSecond = 1.0f / SecondsPerFrame;

    f32 MSPerFrame = SecondsPerFrame / 1000.0f;
    Window->Base.DeltaTime = MSPerFrame - Window->LastMSPerFrame;
    Window->LastMSPerFrame = MSPerFrame;

    // NSLog(@("FPS: %f"), Window->Base.FramesPerSecond);

    Window->LastCounter = EndOfFrameTime;
    return (Window->Base.IsRunning);
}

b32 MacOS_Window_PollEvents(chk_macos_app_window* Window)
{
    if (!Window) { return (false); }

    chk_app_input* Input = Window->Base.Input;
    for (umm GamepadIndex = 0; GamepadIndex < ArrayCount(Input->Gamepads); ++GamepadIndex)
    {
        chk_gamepad_input* Gamepad = &Input->Gamepads[GamepadIndex];
        MacOS_Gamepad_Update(Gamepad);
    }

    NSEvent* Event;
    do {
        Event = [NSApp nextEventMatchingMask:NSEventMaskAny
                                   untilDate:nil
                                      inMode:NSDefaultRunLoopMode
                                     dequeue:YES];

        switch ([Event type])
        {
            case NSEventTypeKeyUp: MacOS_Gamepad_UpdateAsKeyboard(&Input->Gamepads[0], Event.keyCode, false); break;
            case NSEventTypeKeyDown: MacOS_Gamepad_UpdateAsKeyboard(&Input->Gamepads[0], Event.keyCode, true); break;
            default: [NSApp sendEvent:Event]; break;
        }
    } while (Event != nil);

    return (Window->Base.IsRunning);
}

b32 MacOS_Window_MainLoop(chk_macos_app_window* Window)
{
    if (!Window) { return (false); }

    while (Window->Base.IsRunning)
    {
        MacOS_Window_BeginFrame(Window);
        MacOS_Window_EndFrame(Window);
        MacOS_Window_PollEvents(Window);
    }

    return (Window->Base.IsRunning);
}

// Callbacks
b32 MacOS_Window_SetOnSize(chk_macos_app_window* Window, chk_app_window_callback* Callback, void* UserPtr)
{
    if (!Window) { return (false); }

    Window->Base.m_CB_OnSize = Callback;
    Window->Base.m_UP_OnSize = UserPtr;

    return (true);
}

b32 MacOS_Window_SetOnDraw(chk_macos_app_window* Window, chk_app_window_callback* Callback, void* UserPtr)
{
    if (!Window) { return (false); }

    Window->Base.m_CB_OnDraw = Callback;
    Window->Base.m_UP_OnDraw = UserPtr;

    return (true);
}

// Objective-C Madness
@implementation chk_macos_app_window_delegate
- (instancetype)initWithWindow:(struct chk_macos_app_window*)initWindow
{
    self = [super init];
    if (self != nil) { Window = initWindow; }
    return (self);
}
- (void)windowWillClose:(id)Sender
{
    Window->Base.IsRunning = false;
}
- (void)windowDidResize:(NSNotification*)Notification
{
    NSWindow* Sender = (NSWindow*)Notification.object;
    if (Window->Internal == Sender)
    {
        Window->Base.W = (s32)Sender.contentView.bounds.size.width;
        Window->Base.H = (s32)Sender.contentView.bounds.size.height;

        MacOS_Window_ResizeBitmap(Window, Window->Base.W, Window->Base.H);
        if (Window->Base.m_CB_OnSize) { Window->Base.m_CB_OnSize(&Window->Base, Window->Base.m_UP_OnSize); }
        if (Window->Base.m_CB_OnDraw) { Window->Base.m_CB_OnDraw(&Window->Base, Window->Base.m_UP_OnDraw); }
        MacOS_Window_RenderBitmap(Window);
    }
}
@end