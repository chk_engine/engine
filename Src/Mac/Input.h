#pragma once

#include "../Common/Input.h"
#include "Gamepad.h"

typedef struct chk_macos_app_input_stream
{
    chk_app_input* Input;
    umm InputCount;

    b32 IsRecording;
    umm PlayIndex;
    umm RecordIndex;
} chk_macos_app_input_stream;
