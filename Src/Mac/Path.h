#pragma once

#include "../Common/Types.h"

#define CHK_MACOS_MAX_FILENAME_SIZE 4096

typedef struct chk_macos_bundle_info
{
    b32 IsValid;

    char Filename[CHK_MACOS_MAX_FILENAME_SIZE];
    char Data[CHK_MACOS_MAX_FILENAME_SIZE];

    umm FilenameCount;
    umm ContentsCount;
    umm DataCount;

    char* LastSlash;
    char* SecondLastSlash;
} chk_macos_bundle_info;

b32 MacOS_Bundle_Setup(chk_macos_bundle_info* BundleInfo);
b32 MacOS_Bundle_PathRelativeToContents(chk_macos_bundle_info* BundleInfo, const char* Path, umm PathCount, char* Dst, umm DstCount);
b32 MacOS_Bundle_PathRelativeToExecutable(chk_macos_bundle_info* BundleInfo, const char* Path, umm PathCount, char* Dst, umm DstCount);
b32 MacOS_Bundle_PathRelativeToData(chk_macos_bundle_info* BundleInfo, const char* Path, umm PathCount, char* Dst, umm DstCount);

// String
b32 CatStrings(const char* StrA, umm CountA, const char* StrB, umm CountB, char* StrDst, umm CountDst);
umm StringLength(const char* Str);
