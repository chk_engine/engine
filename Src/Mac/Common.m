#include "Common.h"

// Implement logging
#include <sys/sysctl.h>

b32 CHKP_IsDebuggerPresent()
{
    int Mib[4];
    struct kinfo_proc Info;
    size_t Size;

    Info.kp_proc.p_flag = 0;
    Mib[0] = CTL_KERN;
    Mib[1] = KERN_PROC;
    Mib[2] = KERN_PROC_PID;
    Mib[3] = getpid();

    Size = sizeof(Info);
    sysctl(Mib, ArrayCount(Mib), &Info, &Size, NULL, 0);

    return ((Info.kp_proc.p_flag & P_TRACED) != 0);
}

CHK_LOG_FUNC(CHKP_LogImp)
{
    if (CHKP_IsDebuggerPresent())
    {
        fprintf(stderr, ("[%s] %s(%zu) :: %s\n +----> %s\n"), Sender, File, Line, Func, Msg);
    }
}

CHK_LOGFORMAT_FUNC(CHKP_LogFormatImp)
{
    char FMsg[1024];
    va_list Args;
    va_start(Args, Line);
    vsprintf(FMsg, Msg, Args);
    va_end(Args);

    CHKP_LogImp(Sender, FMsg, File, Func, Line);
}

// Implement check
CHK_CHECK_FUNC(CHKP_CheckImp)
{
    __builtin_trap();
}
