#pragma once

#include "../Common/Gamepad.h"
#include "../Common/Types.h"
#include "KeyCodes.h"

b32 MacOS_Gamepad_Setup(chk_gamepad_input* Gamepad);
b32 MacOS_Gamepad_Update(chk_gamepad_input* Gamepad);
b32 MacOS_Gamepad_UpdateAsKeyboard(chk_gamepad_input* Gamepad, u16 KeyCode, b32 State);
