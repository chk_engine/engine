#include "Code.h"

#include <dlfcn.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

// Stubs
CHK_PROCESS_FRAME(Stub_ProcessFrame)
{
    (void)Thread;
    (void)Engine;
}

CHK_PROCESS_AUDIO(Stub_ProcessAudio)
{
    (void)Thread;
    (void)Engine;
}

b32 MacOS_CopyFileToTemp(const char* PathA, char* PathB)
{
    char Buffer[Kilobytes(32)];
    FILE* FileA = fopen(PathA, "rb");
    if (!FileA) { return (false); }
    int FileTemp = mkstemp(PathB);
    FILE* FileB = fdopen(FileTemp, "wb");

    if (!FileB)
    {
        fclose(FileA);
        return (false);
    }

    size_t N;
    while ((N = fread(Buffer, sizeof(char), sizeof(Buffer), FileA)) > 0)
    {
        if (fwrite(Buffer, sizeof(char), N, FileB) != N)
        {
            fclose(FileA);
            fclose(FileB);
            return (false);
        }
    }

    fclose(FileA);
    fclose(FileB);
    return (true);
}

b32 MacOS_Code_Reset(chk_macos_app_code* Code)
{
    if (!Code) { return (false); }

    Code->ProcessFrame = Stub_ProcessFrame;
    Code->ProcessAudio = Stub_ProcessAudio;
    Code->IsValid = false;

    return (true);
}

time_t MacOS_Code_GetWriteTime(chk_macos_app_code* Code)
{
    time_t WriteTime = 0;

    struct stat StatData = {};

    if (stat(Code->Filename, &StatData) == 0)
    {
        WriteTime = StatData.st_mtime;
    }

    return (WriteTime);
}

b32 MacOS_Code_Load(chk_macos_app_code* Code)
{
    MacOS_Code_Reset(Code);

    // Generate temporary path
    const char TempPath[] = "Temp/TmpLibrary_XXXXXX";
    MacOS_Bundle_PathRelativeToExecutable(Code->BundleInfo, TempPath, ArrayCount(TempPath), Code->Tempname, ArrayCount(Code->Tempname));
    MacOS_CopyFileToTemp(Code->Filename, Code->Tempname);

    Code->Library = dlopen(Code->Filename, RTLD_NOW);
    if (!Code->Library) { return (false); }

    Code->ProcessFrame = (chk_process_frame_func*)dlsym(Code->Library, "ProcessFrame");
    if (!Code->ProcessFrame) { Code->ProcessFrame = Stub_ProcessFrame; }

    Code->ProcessAudio = (chk_process_audio_func*)dlsym(Code->Library, "ProcessAudio");
    if (!Code->ProcessAudio) { Code->ProcessAudio = Stub_ProcessAudio; }

    Code->IsValid = (Code->ProcessFrame != Stub_ProcessFrame &&
                     Code->ProcessAudio != Stub_ProcessAudio);

    return (Code->IsValid);
}

b32 MacOS_Code_Setup(chk_macos_app_code* Code)
{
    if (!Code) { return (false); }

    MacOS_Code_ReloadIfNewer(Code);

    if (!Code->IsValid) { MacOS_Code_Reset(Code); }
    return (Code->IsValid);
}

b32 MacOS_Code_Destroy(chk_macos_app_code* Code)
{
    if (!Code || !Code->Library) { return (false); }
    dlclose(Code->Library);
    unlink(Code->Tempname);

    Code->Library = NULL;
    MacOS_Code_Reset(Code);

    return (MacOS_Code_Reset(Code));
}

b32 MacOS_Code_ReloadIfNewer(chk_macos_app_code* Code)
{
    if (!Code) { return (false); }

    time_t WriteTime = MacOS_Code_GetWriteTime(Code);
    if (Code->LastWriteTime < WriteTime)
    {
        if (Code->LastWriteTime == 0)
        {
            LOG_INFO("Loading App code");
        } else {
            LOG_INFO("Reloading App code from a newer version!");
        }
        MacOS_Code_Destroy(Code);
        MacOS_Code_Load(Code);
        Code->LastWriteTime = WriteTime;
    }

    return (true);
}
