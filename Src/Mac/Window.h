#pragma once

#include "../Common/Bitmap.h"
#include "../Common/Window.h"

#import <AppKit/Appkit.h>
#include <mach/mach_init.h>
#include <mach/mach_time.h>

struct chk_macos_window;
@interface chk_macos_app_window_delegate : NSObject <NSWindowDelegate> {
    struct chk_macos_app_window* Window;
}
- (instancetype)initWithWindow:(struct chk_macos_app_window*)initWindow;
@end

typedef struct chk_macos_app_window
{
    chk_app_window Base;

    // MacOS Stuff
    chk_macos_app_window_delegate* Delegate;
    mach_timebase_info_data_t TimeBase;
    u64 LastCounter;
    f32 LastMSPerFrame;

    NSWindow* Internal;
} chk_macos_app_window;

b32 MacOS_Window_Setup(chk_macos_app_window* Window, const char* Caption);
b32 MacOS_Window_Destroy(chk_macos_app_window* Window);
b32 MacOS_Window_BeginFrame(chk_macos_app_window* Window);
b32 MacOS_Window_EndFrame(chk_macos_app_window* Window);
b32 MacOS_Window_PollEvents(chk_macos_app_window* Window);
b32 MacOS_Window_MainLoop(chk_macos_app_window* Window);

// Callbacks
b32 MacOS_Window_SetOnSize(chk_macos_app_window* Window, chk_app_window_callback* Callback, void* UserPtr);
b32 MacOS_Window_SetOnDraw(chk_macos_app_window* Window, chk_app_window_callback* Callback, void* UserPtr);
