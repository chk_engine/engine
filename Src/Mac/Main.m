#include "Common.h"
#include "DebugIO.h"

CHK_WINDOW_CALLBACK(OnDraw)
{
    (void)Window;

    chk_macos_app_engine* MacEngine = UserPtr;
    chk_macos_app_code* Code = MacEngine->Code;
    chk_app_engine* Engine = &MacEngine->Base;
    chk_app_config* Config = Engine->Config;
    // chk_app_input* Input = Engine->Input;

    // Try to reload the engine code
    MacOS_Code_ReloadIfNewer(Code);

    MacOS_Audio_WriteSquareWave((chk_macos_app_audio_output*)Engine->Audio,
                                Config->Frequency, Config->Volume);
    if (Code)
    {
        chk_thread_context Thread = {0};
        Code->ProcessFrame(&Thread, Engine);
        Code->ProcessAudio(&Thread, Engine);
    }
}

s32 main()
{
    // Bundle info
    chk_macos_bundle_info BundleInfo = {0};
    MacOS_Bundle_Setup(&BundleInfo);

    // Test loading a file
    chk_debug_file BmpFile;
    Debug_LoadFile("test.bmp", &BmpFile);

    // Input
    chk_app_input Input = {0};

    // Window
    chk_macos_app_window Window = {0};
    Window.Base.W = 1280;
    Window.Base.H = 720;
    MacOS_Window_Setup(&Window, "chk_engine");
    Window.Base.Input = &Input;

    // Gamepad
    Window.Base.Input->Gamepads[0].Active = true;
    MacOS_Gamepad_Setup(&Window.Base.Input->Gamepads[1]);

    // Audio
    chk_macos_app_audio_output Audio = {0};
    Audio.Base.NumberOfChannels = 2;
    Audio.Base.SamplesPerSecond = 48000;
    Audio.Base.BitsPerChannel = 16;
    MacOS_Audio_Setup(&Audio);

    // Memory
    chk_macos_app_memory Memory = {0};
    Memory.Base.MainSize = Megabytes(256);
    Memory.Base.TempSize = Gigabytes(1);
    MacOS_Memory_Setup(&Memory);

    // Config
    chk_app_config Config = {0};
    Config.CurrentGamepad = 0;
    Config.Frequency = 250;
    Config.Volume = 0.125f;

    // DebugIO
    chk_debug_io DebugIO = {0};
    DebugIO.LoadFile = Debug_LoadFile;
    DebugIO.WriteFile = Debug_WriteFile;
    DebugIO.FreeFile = Debug_FreeFile;

    // Code
    chk_macos_app_code Code = {0};
    Code.BundleInfo = &BundleInfo;
    const char AppCodePath[] = "libgame.dylib";
    MacOS_Bundle_PathRelativeToExecutable(&BundleInfo, AppCodePath, ArrayCount(AppCodePath),
                                          Code.Filename, ArrayCount(Code.Filename));
    MacOS_Code_Setup(&Code);

    // Engine
    chk_macos_app_engine Engine = {0};
    Engine.Base.Window = &Window.Base;
    Engine.Base.Audio = &Audio.Base;
    Engine.Base.Memory = &Memory.Base;
    Engine.Base.Config = &Config;
    Engine.Base.Input = &Input;
    Engine.Code = &Code;

    // Debug Stuff
    chk_debug_logger DebugLogger = {0};
    DebugLogger.Log = CHKP_LogImp;
    DebugLogger.LogFormat = CHKP_LogFormatImp;

    chk_debug_checker DebugChecker = {0};
    DebugChecker.Check = CHKP_CheckImp;

    Engine.Base.m_DebugIO = &DebugIO;
    Engine.Base.m_DebugChecker = &DebugChecker;
    Engine.Base.m_DebugLogger = &DebugLogger;

    // Callbacks
    MacOS_Window_SetOnDraw(&Window, OnDraw, &Engine);

    // Run the program
    MacOS_Window_MainLoop(&Window);

    // Cleanup
    MacOS_Code_Destroy(&Code);
    MacOS_Memory_Destroy(&Memory);
    MacOS_Window_Destroy(&Window);
}
