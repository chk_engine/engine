#include "Gamepad.h"
#include "../Common/Logging.h"

#import <Foundation/Foundation.h>
#import <IOKit/hid/IOHIDLib.h>

void MacOS_Gamepad_Process(void* Context, IOReturn Result, void* Sender, IOHIDValueRef Value)
{
    (void)Sender;
    if (Result != kIOReturnSuccess) { return; }

    chk_gamepad_input* Gamepad = Context;

    IOHIDElementRef Element = IOHIDValueGetElement(Value);
    u32 UsagePage = IOHIDElementGetUsagePage(Element);
    u32 Usage = IOHIDElementGetUsage(Element);

    // Buttons
    if (UsagePage == kHIDPage_Button)
    {
        BOOL ButtonState = (BOOL)IOHIDValueGetIntegerValue(Value);

        if (Usage == Gamepad->RightU.Id)
        {
            Gamepad->RightU.State = ButtonState;
        } else if (Usage == Gamepad->RightL.Id)
        {
            Gamepad->RightL.State = ButtonState;
        } else if (Usage == Gamepad->RightR.Id)
        {
            Gamepad->RightR.State = ButtonState;
        } else if (Usage == Gamepad->RightD.Id)
        {
            Gamepad->RightD.State = ButtonState;
        } else if (Usage == Gamepad->L1.Id)
        {
            Gamepad->L1.State = ButtonState;
        } else if (Usage == Gamepad->R1.Id)
        {
            Gamepad->R1.State = ButtonState;
        }
    } else if (UsagePage == kHIDPage_GenericDesktop)
    {
        f64 Analog = IOHIDValueGetScaledValue(Value, kIOHIDValueScaleTypeCalibrated);

        if (Usage == Gamepad->LStickX.Id)
        {
            Gamepad->LStickX.State = (f32)Analog;
        }

        else if (Usage == Gamepad->LStickY.Id)
        {
            Gamepad->LStickY.State = (f32)Analog;
        }

        else if (Usage == kHIDUsage_GD_Hatswitch) {
            int DPadState = (int)IOHIDValueGetIntegerValue(Value);
            s32 DPadX = 0, DPadY = 0;

            switch (DPadState)
            {
                case 0: DPadX = 0, DPadY = 1; break;
                case 1: DPadX = 1, DPadY = 1; break;
                case 2: DPadX = 1, DPadY = 0; break;
                case 3: DPadX = 1, DPadY = -1; break;
                case 4: DPadX = 0, DPadY = -1; break;
                case 5: DPadX = -1, DPadY = -1; break;
                case 6: DPadX = -1, DPadY = 0; break;
                case 7: DPadX = -1, DPadY = 1; break;
                default: DPadX = 0, DPadY = 0; break;
            }

            Gamepad->LeftL.State = (DPadX < 0);
            Gamepad->LeftR.State = (DPadX > 0);
            Gamepad->LeftU.State = (DPadY < 0);
            Gamepad->LeftD.State = (DPadY > 0);
        }
    }
}

void MacOS_Gamepad_Connected(void* Context, IOReturn Result, void* Sender, IOHIDDeviceRef Device)
{
    (void)Sender;
    if (Result != kIOReturnSuccess) { return; }

    NSUInteger VendorID = [(__bridge NSNumber*)IOHIDDeviceGetProperty(Device,
                                                                      CFSTR(kIOHIDVendorIDKey)) unsignedIntegerValue];
    NSUInteger ProductID = [(__bridge NSNumber*)IOHIDDeviceGetProperty(Device,
                                                                       CFSTR(kIOHIDProductIDKey)) unsignedIntegerValue];

    chk_gamepad_input* Gamepad = Context;
    if (VendorID == 0x054C && ProductID == 0x5C4)
    {
        // PS4 Controller
        LOG_INFO("Sony Dualshock 4 detected.");
        Gamepad->RightD.Id = 0x02;
        Gamepad->RightL.Id = 0x01;
        Gamepad->RightU.Id = 0x04;
        Gamepad->RightR.Id = 0x03;
        Gamepad->L1.Id = 0x05;
        Gamepad->R1.Id = 0x06;

        Gamepad->LStickX.Id = kHIDUsage_GD_X;
        Gamepad->LStickY.Id = kHIDUsage_GD_Y;
    }

    Gamepad->LStickX.State = 128.0f;
    Gamepad->LStickY.State = 128.0f;
    Gamepad->RStickX.State = 128.0f;
    Gamepad->RStickY.State = 128.0f;

    IOHIDDeviceRegisterInputValueCallback(Device, MacOS_Gamepad_Process, (void*)Gamepad);
    IOHIDDeviceSetInputValueMatchingMultiple(Device, (__bridge CFArrayRef) @[
        @{@(kIOHIDElementUsagePageKey) : @(kHIDPage_GenericDesktop)},
        @{@(kIOHIDElementUsagePageKey) : @(kHIDPage_Button)},
    ]);
}

b32 MacOS_Gamepad_Setup(chk_gamepad_input* Gamepad)
{
    IOHIDManagerRef HIDManager = IOHIDManagerCreate(kCFAllocatorDefault, 0);

    if (IOHIDManagerOpen(HIDManager, kIOHIDOptionsTypeNone) != kIOReturnSuccess)
    {
        LOG_INFO("Error Initializing Gamepads");
        return (false);
    }

    IOHIDManagerRegisterDeviceMatchingCallback(HIDManager, MacOS_Gamepad_Connected, (void*)Gamepad);

    IOHIDManagerSetDeviceMatchingMultiple(HIDManager, (__bridge CFArrayRef) @[
        @{@(kIOHIDDeviceUsagePageKey) : @(kHIDPage_GenericDesktop),
          @(kIOHIDDeviceUsageKey) : @(kHIDUsage_GD_GamePad)},
        @{@(kIOHIDDeviceUsagePageKey) : @(kHIDPage_GenericDesktop),
          @(kIOHIDDeviceUsageKey) : @(kHIDUsage_GD_MultiAxisController)},
    ]);

    IOHIDManagerScheduleWithRunLoop(HIDManager,
                                    CFRunLoopGetMain(),
                                    kCFRunLoopDefaultMode);
    return (true);
}

b32 MacOS_Gamepad_Update(chk_gamepad_input* Gamepad)
{
    if (!Gamepad) { return (false); }
    
#define CHK_MACOS_UPDATE_GAMEPAD_BUTTON(Btn)                              \
    do {                                                                  \
        Gamepad->Btn.Pressed = Gamepad->Btn.State && !Gamepad->Btn.Last;  \
        Gamepad->Btn.Released = !Gamepad->Btn.State && Gamepad->Btn.Last; \
        Gamepad->Btn.Last = Gamepad->Btn.State;                           \
    } while(0)

    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(LeftU);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(LeftL);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(LeftR);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(LeftD);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(RightU);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(RightL);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(RightR);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(RightD);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(Start);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(Select);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(Home);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(L1);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(L3);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(R1);
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(R3);

#undef CHK_MACOS_UPDATE_GAMEPAD_BUTTON

    return (false);
}

void MacOS_Gamepad_UpdateAsKeyboard_Stick(chk_gamepad_axis* Axis, u16 State, b32 IsPositive)
{
    if (!Axis) { return; }

    // Check if the state is neutral or not
    if (Axis->State == 128)
    {
       Axis->State = (f32)((State) ? ((IsPositive) ? 255 : 0) : 128);
    } else {
        // If it's not neutral, go to neutral if reversed
        if (IsPositive && Axis->State < 128)
        {
            Axis->State = 128;
        } else if (!IsPositive && Axis->State > 128) {
            Axis->State = 128;
        } else
        {
            Axis->State = (f32)((State) ? ((IsPositive) ? 255 : 0) : 128);
        }
    }
}

b32 MacOS_Gamepad_UpdateAsKeyboard(chk_gamepad_input* Gamepad, u16 KeyCode, b32 State)
{
    if (!Gamepad) { return(false); }
    
    switch (KeyCode)
    {
        // DPad
        case kVK_UpArrow: Gamepad->LeftU.State = State; break;
        case kVK_LeftArrow: Gamepad->LeftL.State = State; break;
        case kVK_RightArrow: Gamepad->LeftR.State = State; break;
        case kVK_DownArrow: Gamepad->LeftD.State = State; break;

        // Buttons
        case kVK_ANSI_I: Gamepad->RightU.State = State; break;
        case kVK_ANSI_J: Gamepad->RightL.State = State; break;
        case kVK_ANSI_L: Gamepad->RightR.State = State; break;
        case kVK_ANSI_K: Gamepad->RightD.State = State; break;

        // L1/R1
        case kVK_ANSI_U: Gamepad->L1.State = State; break;
        case kVK_ANSI_O: Gamepad->R1.State = State; break;

        // Left Analog
        case kVK_ANSI_W: MacOS_Gamepad_UpdateAsKeyboard_Stick(&Gamepad->LStickY, State, false); break;
        case kVK_ANSI_A: MacOS_Gamepad_UpdateAsKeyboard_Stick(&Gamepad->LStickX, State, false); break;
        case kVK_ANSI_D: MacOS_Gamepad_UpdateAsKeyboard_Stick(&Gamepad->LStickX, State, true); break;
        case kVK_ANSI_S: MacOS_Gamepad_UpdateAsKeyboard_Stick(&Gamepad->LStickY, State, true); break;

        default: break;
    }

    return (true);
}
