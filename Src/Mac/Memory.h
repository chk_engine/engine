#pragma once

#include "../Common/Memory.h"
#include "../Common/Types.h"

// Memory
typedef struct chk_macos_app_memory
{
    chk_app_memory Base;
    b32 IsValid;
} chk_macos_app_memory;

b32 MacOS_Memory_Setup(chk_macos_app_memory* Memory);
b32 MacOS_Memory_Destroy(chk_macos_app_memory* Memory);
