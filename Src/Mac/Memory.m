#include "Memory.h"
#include "../Common/Logging.h"
#import <Foundation/Foundation.h>

b32 MacOS_Memory_Setup(chk_macos_app_memory* Memory)
{
    if (!Memory) { return (false); }
    Memory->IsValid = false;

#ifdef CHK_DEVELOPMENT
    u8* BaseAddress = (u8*)Gigabytes(8);
    s32 AllocationFlags = MAP_PRIVATE | MAP_ANON | MAP_FIXED;
#else
    u8* BaseAddress = 0;
    s32 AllocationFlags = MAP_PRIVATE | MAP_ANON;
#endif
    s32 AccessFlags = PROT_READ | PROT_WRITE;

    Memory->Base.Main = mmap(BaseAddress, Memory->Base.MainSize, AccessFlags, AllocationFlags, -1, 0);
    if (Memory->Base.Main == MAP_FAILED)
    {
        LOGF_ERROR("mmap: %d  %s", errno, strerror(errno));
        return (false);
    }

    u8* TempAddress = ((u8*)Memory->Base.Main + Memory->Base.MainSize);
    Memory->Base.Temp = mmap(TempAddress, Memory->Base.TempSize, AccessFlags, AllocationFlags, -1, 0);
    if (Memory->Base.Temp == MAP_FAILED)
    {
        LOGF_ERROR("mmap: %d  %s", errno, strerror(errno));
        return (false);
    }

    Memory->IsValid = true;
    return (Memory->IsValid);
}

b32 MacOS_Memory_Destroy(chk_macos_app_memory* Memory)
{
    if (!Memory || !Memory->IsValid) { return (false); }

    munmap(Memory->Base.Temp, Memory->Base.TempSize);
    munmap(Memory->Base.Main, Memory->Base.MainSize);
    Memory->IsValid = false;
    return (true);
}
