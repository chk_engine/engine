#pragma once

#include "../Common/Platform.h"
#include "../Common/Types.h"
#include "Path.h"

typedef struct chk_macos_app_code
{
    b32 IsValid;
    void* Library;

    chk_process_frame_func* ProcessFrame;
    chk_process_audio_func* ProcessAudio;

    char Tempname[CHK_MACOS_MAX_FILENAME_SIZE];
    char Filename[CHK_MACOS_MAX_FILENAME_SIZE];
    umm LastWriteTime;

    chk_macos_bundle_info* BundleInfo;
} chk_macos_app_code;

b32 MacOS_Code_Setup(chk_macos_app_code* Code);
b32 MacOS_Code_Destroy(chk_macos_app_code* Code);
b32 MacOS_Code_ReloadIfNewer(chk_macos_app_code* Code);
