#pragma once

#include "../Common/Bitmap.h"
#include "../Common/Types.h"

typedef struct chk_app_state
{
    s32 SlimeX, SlimeY;
    chk_bitmap Slime;

} chk_app_state;
