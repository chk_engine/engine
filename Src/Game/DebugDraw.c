//
// Created by Jorge Botarro on 20-10-22.
//

#include "../Common/Util.h"
#include "DebugDraw.h"

void DrawHLine(chk_bitmap* Bmp, u32 Color, s32 X, s32 Y, s32 W)
{
    u8* Row = Bmp->Memory + (X * Bmp->BPP) + (Y * Bmp->Pitch);
    u32* Pixel = (u32*)Row;
    for (s32 DX = 0; DX < W; ++DX)
    {
        *Pixel++ = Color;
    }
}

void DrawVLine(chk_bitmap* Bmp, u32 Color, s32 X, s32 Y, s32 H)
{
    u8* Row = Bmp->Memory + (X * Bmp->BPP) + (Y * Bmp->Pitch);
    for (s32 DY = 0; DY < H; ++DY)
    {
        u32* Pixel = (u32*)Row;
        *Pixel = Color;

        Row += Bmp->Pitch;
    }
}

void DrawRect(chk_bitmap* Bmp, s32 X1, s32 Y1, s32 X2, s32 Y2, u32 Color)
{
    // Mask the alpha
    Color |= (255) << 24;

    if (X2 < X1) { Swap(X1, X2); }
    if (Y2 < Y1) { Swap(Y1, Y2); }

    s32 W = X2 - X1;
    s32 H = Y2 - Y1;

    u8* Row = Bmp->Memory + (X1 * Bmp->BPP) + (Y1 * Bmp->Pitch);
    for (s32 Y = 0; Y < H; ++Y)
    {
        u32* Pixel = (u32*)Row;
        for (s32 X = 0; X < W; ++X)
        {
            *Pixel++ = Color;
        }
        Row += Bmp->Pitch;
    }
}

b32 RenderWeirdGradient(chk_bitmap* Bmp, s32 XOffset, s32 YOffset, b32 Inverted)
{
    if (!Bmp) { return (false); }

    u8* RowPtr = Bmp->Memory;

    u8 Blue = (Inverted) ? 255 : 0;
    u8 Alpha = 255;
    for (s32 Y = 0; Y < Bmp->H; ++Y)
    {
        u8 Green = (u8)(Y + YOffset);

        u32* Pixel = (u32*)RowPtr;
        for (s32 X = 0; X < Bmp->W; ++X)
        {
            u8 Red = (u8)(X + XOffset);
            *Pixel++ = PackRGBA(Red, Green, Blue, Alpha);
        }
        RowPtr += Bmp->Pitch;
    }

    return (true);
}