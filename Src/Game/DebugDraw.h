//
// Created by Jorge Botarro on 20-10-22.
//

#pragma once

#include "../Common/Types.h"
#include "../Common/Bitmap.h"

void DrawHLine(chk_bitmap* Bmp, u32 Color, s32 X, s32 Y, s32 W);
void DrawVLine(chk_bitmap* Bmp, u32 Color, s32 X, s32 Y, s32 H);
void DrawRect(chk_bitmap* Bmp, s32 X1, s32 Y1, s32 X2, s32 Y2, u32 Color);
b32 RenderWeirdGradient(chk_bitmap* Bmp, s32 XOffset, s32 YOffset, b32 Inverted);
