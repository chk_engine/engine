//
// Created by Jorge Botarro on 20-10-22.
//

#pragma once

#include "../Common/Platform.h"
#include "../Common/Types.h"

void RenderDebugAudio(chk_bitmap* Bmp, chk_app_audio_output* Audio);
