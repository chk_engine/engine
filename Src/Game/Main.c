#include "../Common/Platform.h"
#include "Bmp.h"
#include "State.h"
#include "DebugAudio.h"
#include "DebugDraw.h"

MODULE_API CHK_PROCESS_FRAME(ProcessFrame)
{
    (void)Thread;
    chk_bitmap* RenderTarget = &Engine->Window->Bitmap;
    chk_app_memory* Memory = Engine->Memory;
    chk_app_config* Config = Engine->Config;
    chk_debug_io* DebugIO = Engine->m_DebugIO;

    chk_app_state* State = Memory->Main;

    // Initialize the memory if needed
    if (!Memory->Initialized)
    {
        g_Engine = Engine;
        LoadBMP(DebugIO, "slime.bmp", &State->Slime);

        Memory->Initialized = true;
    }

    // Actually process the frame
    RenderWeirdGradient(RenderTarget, Config->XOffset, Config->YOffset, Config->Inverted);

    u32 Color = PackRGB(200, 150, 100);
    DrawRect(RenderTarget, 20, 50, 75, 35, Color);
}

MODULE_API CHK_PROCESS_AUDIO(ProcessAudio)
{
    (void)Thread;
    RenderDebugAudio(&Engine->Window->Bitmap, Engine->Audio);
}
