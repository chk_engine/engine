#pragma once

#include "../Common/Bitmap.h"
#include "../Common/DebugIO.h"
#include "../Common/Types.h"

typedef struct chk_bmp_file_header
{
    u16 TypeSignature; // = "BM"
    u32 Filesize;      // filesize in Bytes
    u32 Reserved;      // = 0 for this program
    u32 HeaderOffset;  // = 54
} chk_bmp_file_header;

typedef struct chk_bmp_info_header
{
    u32 infoHeaderSize;  // size of header in byte. ( = 40)
    u32 Width;           // width of file in pixels
    u32 Height;          // height of file in pixels
    u16 Colors;          // colorbits per pixel (24 for 3byte RGB)
    u16 bitsPerPixel;    // bits per pixel
    u32 Compression;     // compression mode; 0 for uncompressed.
    u32 SizeImg;         // if biCompress = 0, =0. Else: filesize.
    u32 xPelsPerMeter;   // for output device;
    u32 yPelsPerMeter;   // 0 for this program
    u32 ColorsUsed;      // Colours used; = 0 for all
    u32 ColorsImportant; // num. of used colours, 0 for all
} chk_bmp_info_header;

b32 LoadBMP(chk_debug_io* DebugIO, const char* Path, chk_bitmap* BmpOut);