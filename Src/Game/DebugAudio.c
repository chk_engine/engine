#include "DebugDraw.h"
#include "../Common/Platform.h"

void RenderDebugAudio(chk_bitmap* Bmp, chk_app_audio_output* Audio)
{
    /*
    u32 BPS = Audio->BitsPerChannel / 8;
    u32 SampleSize = Audio->NumberOfChannels * BPS;
    u32 NumOfSamples = (Audio->BufferSize / SampleSize);

    // Map from BufferSize to Bitmap->Width 2/3
    const f32 TwoThirds = 2.0f / 3.0f;
    const f32 BmpW = (f32)Bmp->W * TwoThirds;
    const f32 BmpH = (f32)Bmp->H * TwoThirds;

    const f32 AudW = (f32)Audio->BufferSize;
    f32 SampleMax = 0;

    // Calculate Min and Max Ranges
    u8* SamplePtr = Audio->Buffer;
    for (u32 SampleIndex = 0; SampleIndex < NumOfSamples; ++SampleIndex)
    {
        for(s32 SampleI = 0; SampleI < Audio->NumberOfChannels; ++SampleI)
        {
            s16 SampleRaw = *((s16*)SamplePtr + SampleI);
            f32 SampleVal = (f32)SampleRaw;
            if (SampleVal > SampleMax) { SampleVal = SampleRaw; }
        }
    }

    // Common colors
    const u32 BackgroundColor = PackRGBA(175, 100, 100, 255);
    const u32 BorderColor = PackRGBA(200, 200, 100, 255);
    const u32 CursorColor = PackRGBA(250, 150, 150, 255);
    const u32 WaveLColor = PackRGBA(100, 200, 100, 255);
    const u32 WaveRColor = PackRGBA(100, 100, 200, 255);
    const u32 WaveLBGColor = PackRGBA(050, 100, 050, 255);
    const u32 WaveRBGColor = PackRGBA(050, 050, 100, 255);

    // Calculate the bounds
    const s32 RX = (s32)(((f32)Bmp->W - BmpW) / 2.0f);
    const s32 RY = (s32)(((f32)Bmp->H - BmpH) / 2.0f);

    // Draw the background rectangle borders
    DrawHLine(Bmp, BorderColor, RX - 1, RY - 1, (s32)BmpW + 2);
    DrawHLine(Bmp, BorderColor, RX - 1, RY + 1 + (s32)BmpH, (s32)BmpW + 2);
    DrawVLine(Bmp, BorderColor, RX - 1, RY - 1, (s32)BmpH + 2);
    DrawVLine(Bmp, BorderColor, RX + 1 + (s32)BmpW, RY - 1, (s32)BmpH + 2);

    // Draw the background rectangle
    for (s32 DX = 0; DX < (s32)BmpW; ++DX)
    {
        s32 X = RX+DX;
        s32 SampleTop = RY;

        f32 SampleT = (f32)DX / BmpW;
        SamplePtr = Audio->Buffer + (umm)((f32)SampleSize * SampleT);

        DrawVLine(Bmp, BackgroundColor, X, SampleTop, (s32)BmpH);
        f32 SampleH = (BmpH / (f32)Audio->NumberOfChannels);
        for(s32 SampleI = 0; SampleI < Audio->NumberOfChannels; ++SampleI)
        {
            u32 SampleColor = (SampleI % 2 == 0) ? WaveLColor : WaveRColor;

            s16 SampleRaw = *((s16*)SamplePtr + SampleI);
            f32 SampleVal = (f32)SampleRaw / SampleMax;
            f32 SampleValT = SampleVal / 2.0f + 1.0f;
            s32 SampleValH = (s32)(SampleValT * SampleH);
            s32 SampleValTop = (s32)((f32)SampleTop + ((f32)SampleValH / 2.0f));

            DrawVLine(Bmp, SampleColor, X, SampleValTop, SampleValH);

            SampleTop += (s32)SampleH;
        }
    }

    f32 CursorT = (f32)Audio->PlayCursor / AudW;
    s32 CursorX = RX + (s32)(CursorT * BmpW);
    DrawVLine(Bmp, CursorColor, CursorX, RY, (s32)BmpH);
    DrawVLine(Bmp, CursorColor, CursorX+1, RY, (s32)BmpH);
    DrawVLine(Bmp, CursorColor, CursorX-1, RY, (s32)BmpH);
     */
}