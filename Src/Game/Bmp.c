#include "Bmp.h"

b32 Read_u8(u8** Ptr, umm* Count, u8* ValueOut)
{
    const umm ValueSize = sizeof(u8);
    if (!Ptr || *Count < ValueSize) { return (false); }
    *ValueOut = *(u8*)(*Ptr);
    *Ptr += ValueSize;
    *Count -= ValueSize;
    return (true);
}

b32 Read_u16(u8** Ptr, umm* Count, u16* ValueOut)
{
    const umm ValueSize = sizeof(u16);
    if (!Ptr || *Count < ValueSize) { return (false); }
    *ValueOut = *(u16*)(*Ptr);
    *Ptr += ValueSize;
    *Count -= ValueSize;
    return (true);
}

b32 Read_u32(u8** Ptr, umm* Count, u32* ValueOut)
{
    const umm ValueSize = sizeof(u32);
    if (!Ptr || *Count < ValueSize) { return (false); }
    *ValueOut = *(u32*)(*Ptr);
    *Ptr += ValueSize;
    *Count -= ValueSize;
    return (true);
}

b32 Read_u64(u8** Ptr, umm* Count, u64* ValueOut)
{
    const umm ValueSize = sizeof(u64);
    if (!Ptr || *Count < ValueSize) { return (false); }
    *ValueOut = *(u64*)(*Ptr);
    *Ptr += ValueSize;
    *Count -= ValueSize;
    return (true);
}

b32 LoadBMP(chk_debug_io* DebugIO, const char* Path, chk_bitmap* BmpOut)
{
    if (!BmpOut) { return (false); }
    b32 Result = true;

    // Read the entire file to memory
    chk_debug_file File;
    if (!DebugIO->LoadFile(Path, &File)) { return (false); }
    u8* ReadPtr = File.Memory;
    umm RemainingBytes = File.MemorySize;

    // Read the header
    chk_bmp_file_header FileHeader = {0};
    Read_u16(&ReadPtr, &RemainingBytes, &FileHeader.TypeSignature);
    if (((u8*)&FileHeader.TypeSignature)[0] != 'B' && ((u8*)&FileHeader.TypeSignature)[1] != 'M')
    {
        Result = false;
        goto _LoadBMP_PerformCleanup;
    }

    Read_u32(&ReadPtr, &RemainingBytes, &FileHeader.Filesize);
    Read_u32(&ReadPtr, &RemainingBytes, &FileHeader.Reserved);
    Read_u32(&ReadPtr, &RemainingBytes, &FileHeader.HeaderOffset);

// We're done with the original file!
_LoadBMP_PerformCleanup:
    DebugIO->FreeFile(&File);
    return (Result);
}