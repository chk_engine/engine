#pragma once

#include "../Common/Engine.h"
#include "../Common/Types.h"
#include "Code.h"

typedef struct chk_win_app_engine
{
    chk_app_engine Base;
    chk_win_app_code* Code;

} chk_win_app_engine;
