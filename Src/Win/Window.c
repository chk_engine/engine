#include "Window.h"
#include "Resources.h"

static WNDCLASSEX g_Cls = {0};
static bool g_ClsRegistered = false;
LRESULT CALLBACK Win32_Window_WindowProc(HWND, UINT, WPARAM, LPARAM);

void Win32_Window_Init()
{
    if (g_ClsRegistered) { return; }

    HINSTANCE Instance = GetModuleHandle(NULL);

    // Get the icon dimensions
    s32 IconBigW = GetSystemMetrics(SM_CXICON), IconBigH = GetSystemMetrics(SM_CYICON);
    s32 IconSmlW = GetSystemMetrics(SM_CXSMICON), IconSmlH = GetSystemMetrics(SM_CYSMICON);

    // Get the icons
    HICON IconBig = (HICON)LoadImage(Instance, MAKEINTRESOURCE(IDI_ICON_MAIN), IMAGE_ICON, IconBigW, IconBigH, LR_DEFAULTCOLOR | LR_SHARED);
    HICON IconSml = (HICON)LoadImage(Instance, MAKEINTRESOURCE(IDI_ICON_MAIN), IMAGE_ICON, IconSmlW, IconSmlH, LR_DEFAULTCOLOR | LR_SHARED);

    // Get the cursor
    HCURSOR Cursor = LoadCursor(NULL, IDC_CROSS);

    g_Cls.cbSize = sizeof(g_Cls);
    g_Cls.style = CS_HREDRAW | CS_VREDRAW;
    g_Cls.lpfnWndProc = Win32_Window_WindowProc;
    g_Cls.cbClsExtra = 0;
    g_Cls.cbWndExtra = 0;
    g_Cls.hInstance = Instance;
    g_Cls.hIcon = IconBig;
    g_Cls.hCursor = Cursor;
    g_Cls.hbrBackground = (HBRUSH)(COLOR_BACKGROUND + 1);
    g_Cls.lpszMenuName = NULL;
    g_Cls.lpszClassName = TEXT("chk_dev_main_window");
    g_Cls.hIconSm = IconSml;

    if (!RegisterClassEx(&g_Cls))
    {
        s32 ErrorCode = (s32)GetLastError();
        MessageBox(NULL, TEXT("Error registering the window class!"), TEXT("RegisterClassEx"), MB_ICONERROR | MB_OK);
        ExitProcess(ErrorCode);
    }

    g_ClsRegistered = true;
}

b32 Win32_Window_ResizeBitmap(chk_win_app_window* Window, s32 W, s32 H)
{
    if (!Window) { return (false); }
    chk_bitmap* Bmp = &Window->Base.Bitmap;

    if (Bmp->Memory)
    {
        VirtualFree(Bmp->Memory, 0, MEM_RELEASE);
        Bmp->Memory = NULL;
        Bmp->MemorySize = 0;
    }

    OutputDebugString(_T("Resizing bitmap!\n"));

    Bmp->W = W;
    Bmp->H = H;
    Bmp->BPP = 4;
    Bmp->Pitch = Bmp->W * Bmp->BPP;
    Bmp->MemorySize = Bmp->Pitch * Bmp->H;
    Bmp->Memory = VirtualAlloc(0, Bmp->MemorySize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

    if (!Bmp->Memory)
    {
        Window->Base.Bitmap = (chk_bitmap){0};
        return (false);
    }

    Window->BitmapInfo.bmiHeader.biWidth = W;
    Window->BitmapInfo.bmiHeader.biHeight = -H;

    return (true);
}

b32 Win32_Window_RenderBitmap(chk_win_app_window* Window)
{
    if (!Window) { return (false); }

    chk_bitmap* Bitmap = &Window->Base.Bitmap;
    if (Bitmap->Memory)
    {
        OutputDebugString(_T("Rendering bitmap!\n"));

        HDC DeviceContext = GetDC(Window->Handle);
        StretchDIBits(DeviceContext, 0, 0, Window->Base.W, Window->Base.H, 0, 0, Bitmap->W, Bitmap->H, Bitmap->Memory, &Window->BitmapInfo, DIB_RGB_COLORS, SRCCOPY);
        ReleaseDC(Window->Handle, DeviceContext);
    }

    return (true);
}

// Create/Destroy
b32 Win32_Window_Setup(chk_win_app_window* Window, const char* Caption)
{
    Win32_Window_Init();

    // Resize the requested window size so that it includes the window borders
    DWORD Style = WS_OVERLAPPEDWINDOW;
    DWORD StyleEx = WS_EX_OVERLAPPEDWINDOW;
    RECT WinRect = {0, 0, Window->Base.W, Window->Base.H};
    AdjustWindowRectEx(&WinRect, Style, FALSE, StyleEx);
    int WinRectW = WinRect.right - WinRect.left;
    int WinRectH = WinRect.bottom - WinRect.top;

    Window->BitmapInfo.bmiHeader.biSize = sizeof(Window->BitmapInfo.bmiHeader);
    Window->BitmapInfo.bmiHeader.biWidth = WinRectW;
    Window->BitmapInfo.bmiHeader.biHeight = -WinRectH;
    Window->BitmapInfo.bmiHeader.biPlanes = 1;
    Window->BitmapInfo.bmiHeader.biBitCount = 32;
    Window->BitmapInfo.bmiHeader.biCompression = BI_RGB;
    Window->BitmapInfo.bmiHeader.biSizeImage = 0;
    Window->BitmapInfo.bmiHeader.biXPelsPerMeter = 0;
    Window->BitmapInfo.bmiHeader.biYPelsPerMeter = 0;
    Window->BitmapInfo.bmiHeader.biClrUsed = 0;
    Window->BitmapInfo.bmiHeader.biClrImportant = 0;

    // Load accelerators
    Window->Accelerators = LoadAccelerators(g_Cls.hInstance, MAKEINTRESOURCE(ACC_INGAME_ACTIONS));

#if defined(UNICODE) || defined(_UNICODE)
    TCHAR TCaption[1024];
    DWORD Written = MultiByteToWideChar(CP_UTF8, 0, Caption, -1, TCaption, ArrayCount(TCaption));
    if (Written == 0) { return (false); }
#else
    const char* TCaption = Caption;
#endif

    Window->Handle = CreateWindowEx(StyleEx, g_Cls.lpszClassName, TCaption, Style,
                                    CW_USEDEFAULT, CW_USEDEFAULT, WinRectW, WinRectH,
                                    NULL, NULL, g_Cls.hInstance, Window);
    if (!Window->Handle)
    {
        MessageBox(NULL, _T("Error creating the window!"), _T("CreateWindowEx"), MB_ICONERROR | MB_OK);
        return (false);
    }

    Win32_Window_ResizeBitmap(Window, WinRectW, WinRectH);
    Win32_Window_RenderBitmap(Window);

    s32 CmdShow = SW_SHOWDEFAULT;
    ShowWindow(Window->Handle, CmdShow);
    UpdateWindow(Window->Handle);

    return (true);
}

b32 Win32_Window_Destroy(chk_win_app_window* Window)
{
    if (!Window) { return (false); }

    return (true);
}

// Common operations
b32 Win32_Window_BeginFrame(chk_win_app_window* Window)
{
    if (!Window || !Window->Base.IsRunning) { return (false); }

    Win32_Window_PollEvents(Window);

    return (Window->Base.IsRunning);
}

b32 Win32_Window_EndFrame(chk_win_app_window* Window)
{
    if (!Window || !Window->Base.IsRunning) { return (false); }

    if (Window->Base.m_CB_OnDraw)
    {
        Window->Base.m_CB_OnDraw(&Window->Base, Window->Base.m_UP_OnDraw);
    }

    Win32_Window_RenderBitmap(Window);

    return (Window->Base.IsRunning);
}

b32 Win32_Window_PollEvents(chk_win_app_window* Window)
{
    if (!Window || !Window->Base.IsRunning) { return (false); }

    MSG Msg = {0};
    while (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE) != 0)
    {
        if (!TranslateAccelerator(Window->Handle, Window->Accelerators, &Msg))
        {
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }
    }

    return (Window->Base.IsRunning);
}

b32 Win32_Window_MainLoop(chk_win_app_window* Window)
{
    if (!Window || !Window->Base.IsRunning) { return (false); }

    while (Window->Base.IsRunning)
    {
        if (!Win32_Window_BeginFrame(Window)) { break; }
        Win32_Window_EndFrame(Window);
    }

    return (Window->Base.IsRunning);
}

b32 Win32_Window_ToggleFullscreen(chk_win_app_window* Window)
{
    DWORD Style = (DWORD)GetWindowLong(Window->Handle, GWL_STYLE);
    DWORD StyleEx = (DWORD)GetWindowLong(Window->Handle, GWL_EXSTYLE);
    if (!Window->Base.IsFullscreen)
    {
        // Go fullscreen
        MONITORINFO MonInfo = {sizeof(MonInfo)};
        if (GetWindowPlacement(Window->Handle, &Window->Placement) &&
            GetMonitorInfo(MonitorFromWindow(Window->Handle, MONITOR_DEFAULTTOPRIMARY), &MonInfo))
        {
            Window->SavedStyle = Style;
            Window->SavedStyleEx = StyleEx;

            SetWindowLong(Window->Handle, GWL_STYLE, (LONG)(Style & ~WS_OVERLAPPEDWINDOW));
            SetWindowLong(Window->Handle, GWL_EXSTYLE, (LONG)(StyleEx & ~WS_EX_OVERLAPPEDWINDOW));
            SetWindowPos(Window->Handle, HWND_TOP,
                         MonInfo.rcMonitor.left, MonInfo.rcMonitor.top,
                         MonInfo.rcMonitor.right - MonInfo.rcMonitor.left,
                         MonInfo.rcMonitor.bottom - MonInfo.rcMonitor.top,
                         SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

            Window->Base.IsFullscreen = true;
        } else
        {
            return (false);
        }

    } else
    {
        // Restore from fullscreen
        SetWindowLong(Window->Handle, GWL_STYLE, (s32)Window->SavedStyle);
        SetWindowLong(Window->Handle, GWL_EXSTYLE, (s32)Window->SavedStyleEx);

        SetWindowPlacement(Window->Handle, &Window->Placement);
        SetWindowPos(Window->Handle, NULL, 0, 0, 0, 0,
                     (SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                      SWP_NOOWNERZORDER | SWP_FRAMECHANGED));

        Window->Base.IsFullscreen = false;
    }

    return (true);
}

b32 Win32_Window_SetFullscreen(chk_win_app_window* Window, b32 Value)
{
    if (!Window) { return (false); }

    if (Window->Base.IsFullscreen == Value) { return (false); }
    Win32_Window_ToggleFullscreen(Window);

    return (true);
}

b32 Win32_Window_SetMinimized(chk_win_app_window* Window, b32 Value)
{
    if (Window->Base.IsMinimized == Value) { return (false); }

    if (Value)
    {
        PostMessage(Window->Handle, WM_SYSCOMMAND, SC_MINIMIZE, 0);
    } else
    {
        PostMessage(Window->Handle, WM_SYSCOMMAND, SC_RESTORE, 0);
    }

    return (true);
}

b32 Win32_Window_SetMaximized(chk_win_app_window* Window, b32 Value)
{
    if (Window->Base.IsMinimized == Value) { return (false); }

    if (Value)
    {
        PostMessage(Window->Handle, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
    } else
    {
        PostMessage(Window->Handle, WM_SYSCOMMAND, SC_RESTORE, 0);
    }

    return (true);
}

// Callback setters
b32 Win32_Window_SetOnDraw(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr)
{
    if (!Window) { return (false); }

    Window->Base.m_CB_OnDraw = Callback;
    Window->Base.m_UP_OnDraw = UserPtr;

    return (true);
}

b32 Win32_Window_SetOnMove(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr)
{
    if (!Window) { return (false); }

    Window->Base.m_CB_OnMove = Callback;
    Window->Base.m_UP_OnMove = UserPtr;

    return (true);
}

b32 Win32_Window_SetOnSize(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr)
{
    if (!Window) { return (false); }

    Window->Base.m_CB_OnSize = Callback;
    Window->Base.m_UP_OnSize = UserPtr;

    return (true);
}

b32 Win32_Window_SetOnQuit(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr)
{
    if (!Window) { return (false); }

    Window->Base.m_CB_OnQuit = Callback;
    Window->Base.m_UP_OnQuit = UserPtr;

    return (true);
}

b32 Win32_Window_SetOnFullscreen(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr)
{
    if (!Window) { return (false); }

    Window->Base.m_CB_OnFullscreen = Callback;
    Window->Base.m_UP_OnFullscreen = UserPtr;

    return (true);
}

// Window Callback
LRESULT CALLBACK Win32_Window_WindowProc(HWND Handle, UINT Msg, WPARAM Wp, LPARAM Lp)
{
    LRESULT Result = 0;

#define CHK_GET_WINDOW_PTR(Name)                                                                   \
    chk_win_app_window* Name = (chk_win_app_window*)GetWindowLongPtr(Handle, GWLP_USERDATA); \
    if (!Window)                                                                               \
    {                                                                                          \
        Result = DefWindowProc(Handle, Msg, Wp, Lp);                                           \
        break;                                                                                 \
    }

    switch (Msg)
    {
        case WM_NCCREATE:
        {
            // Initialize the WindowData internal pointer
            CREATESTRUCT* Cs = (CREATESTRUCT*)Lp;
            chk_win_app_window* Window = (chk_win_app_window*)Cs->lpCreateParams;
            Window->Base.IsRunning = true;
            SetWindowLongPtr(Handle, GWLP_USERDATA, (LONG_PTR)Window);
            Window->Handle = Handle;

            // Get the real windows version
            // winData->m_verInfo = WinVer_Get();

            // Get the current dark mode state
            // UserDarkMode_Update(&winData->data.darkMode, winData->handle);

            // Get the current accent color
            // winData->Data().AccentColors().Update();

            Result = DefWindowProc(Handle, Msg, Wp, Lp);
        }
        break;

        case WM_CLOSE:
        {
            CHK_GET_WINDOW_PTR(Window)

            b32 ShouldDestroy = true;
            if (Window->HasClosingDialog)
            {
                int Response = MessageBox(Window->Handle, TEXT("Do you really want to quit?"), TEXT("Quit?"), MB_YESNO | MB_ICONQUESTION);
                if (Response != IDYES) { ShouldDestroy = false; }
            }

            if (ShouldDestroy)
            {
                DestroyWindow(Window->Handle);
            }
        }
        break;

        case WM_DESTROY:
        {
            CHK_GET_WINDOW_PTR(Window)

            Window->Base.IsRunning = false;
        }
        break;

        case WM_MOVE:
        {
            CHK_GET_WINDOW_PTR(Window)

            s32 NewX = (s32)(s16)LOWORD(Lp);
            s32 NewY = (s32)(s16)HIWORD(Lp);

            b32 HasMoved = (NewX != Window->Base.X || NewY != Window->Base.Y);
            if (HasMoved)
            {
                Window->Base.X = NewX;
                Window->Base.Y = NewY;

                if (Window->Base.m_CB_OnMove) { Window->Base.m_CB_OnMove(&Window->Base, Window->Base.m_UP_OnMove); }
            }
        }
        break;

        case WM_SIZE:
        {
            CHK_GET_WINDOW_PTR(Window)

            b32 StateChanged = false;
            switch (Wp)
            {
                case SIZE_MAXIMIZED:
                {
                    Window->Base.IsMaximized = true;
                    StateChanged = true;
                }
                break;
                case SIZE_MINIMIZED:
                {
                    Window->Base.IsMinimized = true;
                    StateChanged = true;
                }
                break;
                case SIZE_RESTORED:
                {
                    Window->Base.IsMinimized = Window->Base.IsMinimized = false;
                    StateChanged = true;
                }
                break;

                default: break;
            }

            s32 NewW = LOWORD(Lp);
            s32 NewH = HIWORD(Lp);
            b32 HasResized = (StateChanged || Window->Base.W != NewW || Window->Base.H != NewH);

            if (HasResized)
            {
                Window->Base.W = NewW;
                Window->Base.H = NewH;

                Win32_Window_ResizeBitmap(Window, NewW, NewH);
                if (Window->Base.m_CB_OnSize) { Window->Base.m_CB_OnSize(&Window->Base, Window->Base.m_UP_OnSize); }
                if (Window->Base.m_CB_OnDraw) { Window->Base.m_CB_OnDraw(&Window->Base, Window->Base.m_UP_OnDraw); }
                Win32_Window_RenderBitmap(Window);
            }
        }
        break;

        case WM_ACTIVATE:
        {
            CHK_GET_WINDOW_PTR(Window)

            HWND Target = (HWND)Lp;
            if (Target == Window->Handle)
            {
                b32 State = LOWORD(Wp);
                Window->Base.IsFocused = !State;
            }
        }
        break;

        case WM_ERASEBKGND:
        {
            CHK_GET_WINDOW_PTR(Window)

            Result = TRUE;
        }
        break;

        case WM_PAINT:
        {
            CHK_GET_WINDOW_PTR(Window)

            PAINTSTRUCT ps;
            BeginPaint(Window->Handle, &ps);
            Win32_Window_RenderBitmap(Window);
            EndPaint(Window->Handle, &ps);
        }
        break;

        case WM_DWMCOLORIZATIONCOLORCHANGED:
        {
            CHK_GET_WINDOW_PTR(Window)

            // UserAccentColors_ApplyRaw(&winData->data.accentColors, (DWORD)wp);

            InvalidateRect(Window->Handle, NULL, TRUE);
            UpdateWindow(Window->Handle);

            Result = DefWindowProc(Handle, Msg, Wp, Lp);
        }
        break;

        case WM_SETTINGCHANGE:
        {
            CHK_GET_WINDOW_PTR(Window)

            // Look for dark mode notifications only if we're in a modern version of windows
            if (Window->VerInfo.dwBuildNumber > 17763)
            {
                if (_tcscmp((LPTSTR)Lp, _T("ImmersiveColorSet")) == 0)
                {
                    // Dark mode toggled! get the registry key value
                    // b32 isDarkMode = UserDarkMode_Get(&winData->data.darkMode);
                    // UserDarkMode_Set(&winData->data.darkMode, winData->handle, isDarkMode);
                }
            }

            // Do the normal thing
            Result = DefWindowProc(Handle, Msg, Wp, Lp);
        }
        break;

        case WM_COMMAND:
        {
            CHK_GET_WINDOW_PTR(Window)

            switch (LOWORD(Wp))
            {
                case ACC_INGAME_TOGGLE_FULLSCREEN:
                {
                    Win32_Window_ToggleFullscreen(Window);
                }
                break;
            }
        }
        break;

        default:
        {
            Result = DefWindowProc(Handle, Msg, Wp, Lp);
        }
        break;
    }

#undef CHK_GET_WINDOW_PTR

    return (Result);
}
