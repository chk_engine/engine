#include "Audio.h"
#include "../Common/Logging.h"

#include <math.h>
#define Pi 3.14159265359f

b32 Win32_Audio_Destroy(chk_win_app_audio_output* SoundOutput)
{
    if (!SoundOutput) { return(false); }

    if (SoundOutput->Base.Buffer) { VirtualFree(SoundOutput->Base.Buffer, 0, MEM_RELEASE); SoundOutput->Base.Buffer = NULL; }
    SoundOutput->Base.BufferSize = 0;

    if (SoundOutput->DSound) { SoundOutput->DSound = NULL; }
    if (SoundOutput->Module) { FreeLibrary(SoundOutput->Module); SoundOutput->Module = NULL; }

    CoUninitialize();
    return(true);
}

b32 Win32_Audio_Init(chk_win_app_audio_output* Audio)
{
    if (!Audio) { return (false); }
    CoInitializeEx(NULL, COINIT_MULTITHREADED);

    Audio->Module = LoadLibrary(_T("dsound.dll"));
    chk_direct_sound_create_func* DSoundCreate = (chk_direct_sound_create_func*)GetProcAddress(Audio->Module, "DirectSoundCreate");

    if (!DSoundCreate) {
        LOG_ERROR("Failed to load DirectSound!");
        Win32_Audio_Destroy(Audio);
        return(false);
    }

    if (FAILED(DSoundCreate(NULL, &Audio->DSound, NULL)))
    {
        LOG_ERROR("DirectSoundCreate failed!");
        Win32_Audio_Destroy(Audio);
        return(false);
    }

    if (FAILED(IDirectSound_SetCooperativeLevel(Audio->DSound, Audio->Handle, DSSCL_PRIORITY)))
    {
        LOG_ERROR("Failed to set the cooperative level!");
        Win32_Audio_Destroy(Audio);
        return(false);
    }

    DSBUFFERDESC BufferDesc = {0};
    BufferDesc.dwSize = sizeof(BufferDesc);
    BufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER;


    if (FAILED(IDirectSound_CreateSoundBuffer(Audio->DSound, &BufferDesc, &Audio->PrimaryBuffer, NULL)))
    {
        LOG_ERROR("Failed to create the Primary Sound Buffer!");
        Win32_Audio_Destroy(Audio);
        return (false);
    }

    WAVEFORMATEX WaveFormat = {0};
    WaveFormat.wFormatTag = WAVE_FORMAT_PCM;
    WaveFormat.nChannels = Audio->Base.NumberOfChannels;
    WaveFormat.nSamplesPerSec = Audio->Base.SamplesPerSecond;
    WaveFormat.wBitsPerSample = Audio->Base.BitsPerChannel;
    WaveFormat.nBlockAlign = (WaveFormat.nChannels * WaveFormat.wBitsPerSample) / 8;
    WaveFormat.nAvgBytesPerSec = WaveFormat.nSamplesPerSec * WaveFormat.nBlockAlign;
    WaveFormat.cbSize = sizeof(WaveFormat);

    if (FAILED(IDirectSoundBuffer_SetFormat(Audio->PrimaryBuffer, (LPCWAVEFORMATEX)&WaveFormat)))
    {
        LOG_ERROR("Failed to set the format of the Sound Buffer!");
        Win32_Audio_Destroy(Audio);
        return(false);
    }

    DSBUFFERDESC BufferDesc2 = {0};
    BufferDesc2.dwSize = sizeof(BufferDesc2);
    BufferDesc2.dwFlags = DSBCAPS_GETCURRENTPOSITION2;
    BufferDesc2.dwBufferBytes = Audio->Base.BufferSize;
    BufferDesc2.lpwfxFormat = &WaveFormat;

    if (FAILED(IDirectSound_CreateSoundBuffer(Audio->DSound, &BufferDesc2, &Audio->SecondaryBuffer, NULL)))
    {
        LOG_ERROR("Failed to create the Secondary Sound Buffer!");
        Win32_Audio_Destroy(Audio);
        return(false);
    }

    LOG_INFO("DSound was initialized!");
    return(true);
}

b32 Win32_Audio_Setup(chk_win_app_audio_output* Audio)
{
    if (!Audio || !Audio->Base.SamplesPerSecond) { return (false); }

    Audio->Base.LatencySampleCount = Audio->Base.SamplesPerSecond / 16;
    Audio->Base.BytesPerSample = Audio->Base.NumberOfChannels * (Audio->Base.BitsPerChannel / 8);

    Audio->Base.BufferSize = Audio->Base.SamplesPerSecond * sizeof(s16) * Audio->Base.NumberOfChannels;
    Audio->Base.Buffer = VirtualAlloc(0, Audio->Base.BufferSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    if (!Audio->Base.Buffer) {
        LOGF_ERROR("Failed to allocate %u bytes for the Sound Buffer!", Audio->Base.BufferSize);
        return(false);
    }

    Win32_Audio_Init(Audio);

    return (true);
}

b32 Win32_Audio_StartPlaying(chk_win_app_audio_output* Audio)
{
    if (!Audio) { return(false); }
    IDirectSoundBuffer_Play(Audio->SecondaryBuffer, 0, 0, DSBPLAY_LOOPING);
    Audio->Base.IsPlaying = true;
    return (true);
}

void Win32_Audio_FillBuffer(chk_win_app_audio_output* Audio, u32 Frequency, f32 Volume, DWORD ByteToLock, DWORD BytesToWrite)
{
    // u32 BytesPerSample = Audio->Base.NumberOfChannels * (Audio->Base.BitsPerChannel / 8);
    f32 WavePeriod = (f32)Audio->Base.SamplesPerSecond / (f32)Frequency;
    f32 MaxVol = 5000.0f * Volume;

    void* Rgn1;
    void* Rgn2;
    DWORD Rgn1Size, Rgn2Size;
    IDirectSoundBuffer_Lock(Audio->SecondaryBuffer, ByteToLock, BytesToWrite, &Rgn1, &Rgn1Size, &Rgn2, &Rgn2Size, 0);

    s16* Rgn1SamplePtr = (s16*)Rgn1;
    s32 Rgn1SampleCount = (s32)Rgn1Size / (s32)Audio->Base.BytesPerSample;
    for(s32 SampleI = 0; SampleI < Rgn1SampleCount; ++SampleI)
    {
        f32 T = ((f32)Audio->Base.RunningSampleIndex / (f32)WavePeriod) * (Pi * 2.0f);
        f32 SinValue = sinf(T);
        s16 SampleValue = (s16)(SinValue * MaxVol);
        *Rgn1SamplePtr++ = SampleValue;
        *Rgn1SamplePtr++ = SampleValue;

        ++Audio->Base.RunningSampleIndex;
    }

    s16* Rgn2SamplePtr = (s16*)Rgn2;
    s32 Rgn2SampleCount = (s32)Rgn2Size / (s32)Audio->Base.BytesPerSample;
    for(s32 SampleI = 0; SampleI < Rgn2SampleCount; ++SampleI)
    {
        f32 T = ((f32)Audio->Base.RunningSampleIndex / (f32)WavePeriod) * (Pi * 2.0f);
        f32 SinValue = sinf(T);
        s16 SampleValue = (s16)(SinValue * MaxVol);
        *Rgn2SamplePtr++ = SampleValue;
        *Rgn2SamplePtr++ = SampleValue;

        ++Audio->Base.RunningSampleIndex;
    }

    IDirectSoundBuffer_Unlock(Audio->SecondaryBuffer, Rgn1, Rgn1Size, Rgn2, Rgn2Size);
}

void Win32_Audio_WriteWave(chk_win_app_audio_output* Audio, u32 Frequency, f32 Volume)
{
    if (!Audio) { return; }


    // Get the Write and Play cursors
    DWORD PlayCursor, WriteCursor;
    if (FAILED(IDirectSoundBuffer_GetCurrentPosition(Audio->SecondaryBuffer, &PlayCursor, &WriteCursor)))
    {
        LOG_WARNING("Failed to get the current playback position!");
        return;
    }

    DWORD ByteToLock = (Audio->Base.RunningSampleIndex * Audio->Base.BytesPerSample) % Audio->Base.BufferSize;
    DWORD TargetCursor = (PlayCursor + (Audio->Base.LatencySampleCount * Audio->Base.BytesPerSample) % Audio->Base.BufferSize);

    DWORD BytesToWrite;
    if (ByteToLock > TargetCursor)
    {
        BytesToWrite = Audio->Base.BufferSize - TargetCursor;
        BytesToWrite += TargetCursor;
    } else
    {
        BytesToWrite = TargetCursor - ByteToLock;
    }

    Win32_Audio_FillBuffer(Audio, Frequency, Volume, ByteToLock, BytesToWrite);
}
