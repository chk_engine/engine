#include "Memory.h"
#include "Win32.h"

b32 Win32_Memory_Setup(chk_win_app_memory* Memory)
{
    if (!Memory) { return (false); }
    Memory->IsValid = false;

    // Align to Large Pages
    umm Alignment = GetLargePageMinimum();
    if (Alignment != 0)
    {
        umm RemA = Memory->Base.MainSize % Alignment;
        umm RemB = Memory->Base.TempSize % Alignment;
        Memory->Base.MainSize = RemA ? Memory->Base.MainSize + (Alignment - RemA) : Memory->Base.MainSize;
        Memory->Base.TempSize = RemB ? Memory->Base.TempSize + (Alignment - RemB) : Memory->Base.TempSize;
    }

#ifdef CHK_DEVELOPMENT
    void* BaseAddress = (u8*)Gigabytes(8);
    u32 AllocationFlags = MEM_RESERVE | MEM_COMMIT;
#else
    void* BaseAddress = 0;
    u32 AllocationFlags = MEM_RESERVE | MEM_COMMIT;
#endif
    u32 AccessFlags = PAGE_READWRITE;

    // Memory->Base.Main = mmap(BaseAddress, Memory->Base.MainSize, AccessFlags, AllocationFlags, -1, 0);
    Memory->Base.Main = VirtualAlloc(BaseAddress, Memory->Base.MainSize, AllocationFlags, AccessFlags);
    if (!Memory->Base.Main)
    {
        DebugBreak();
        ExitProcess(1);
    }

#ifdef CHK_DEVELOPMENT
    u8* TempAddress = ((u8*)Memory->Base.Main + Memory->Base.MainSize);
#else
    u8* TempAddress = 0;
#endif

    Memory->Base.Temp = VirtualAlloc(TempAddress, Memory->Base.TempSize, AllocationFlags, AccessFlags);
    if (!Memory->Base.Temp)
    {
        DebugBreak();
        ExitProcess(1);
    }

    Memory->IsValid = true;
    return (Memory->IsValid);
}

b32 Win32_Memory_Destroy(chk_win_app_memory* Memory)
{
    if (!Memory || !Memory->IsValid) { return (false); }

    VirtualFree(Memory->Base.Temp, Memory->Base.TempSize, MEM_RELEASE);
    VirtualFree(Memory->Base.Main, Memory->Base.MainSize, MEM_RELEASE);
    Memory->IsValid = false;
    return (true);
}
