#include "Common.h"
#include <stdio.h>

// Implement logging
CHK_LOG_FUNC(CHKP_LogImp)
{
    char Buffer[1024];
    sprintf(Buffer, ("[%s] %s(%zu) :: %s\n +----> %s\n"), Sender, File, Line, Func, Msg);

    TCHAR TBuffer[1024];
    DWORD Written = MultiByteToWideChar(CP_UTF8, 0, Buffer, -1, TBuffer, ArrayCount(TBuffer));
    if (Written == 0) { return; }

    OutputDebugString(TBuffer);
}

CHK_LOGFORMAT_FUNC(CHKP_LogFormatImp)
{
    char FMsg[1024];
    va_list Args;
    va_start(Args, Line);
    vsprintf(FMsg, Msg, Args);
    va_end(Args);

    CHKP_LogImp(Sender, FMsg, File, Func, Line);
}

// Implement check
CHK_CHECK_FUNC(CHKP_CheckImp)
{
    DebugBreak();
    ExitProcess(1);
}
