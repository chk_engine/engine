#include "Common.h"
#include "DebugIO.h"

CHK_WINDOW_CALLBACK(OnDraw)
{
    (void)Window;

    chk_win_app_engine* WinEngine = UserPtr;
    chk_win_app_code* Code = WinEngine->Code;
    chk_app_engine* Engine = &WinEngine->Base;
    chk_app_config* Config = Engine->Config;
    // chk_app_input* Input = Engine->Input;

    // Try to reload the engine code
    Win32_Code_ReloadIfNewer(Code);

    Win32_Audio_WriteWave((chk_win_app_audio_output*)Engine->Audio, Config->Frequency, Config->Volume);
    if (Code)
    {
        chk_thread_context Thread = {0};
        Code->ProcessFrame(&Thread, Engine);
        Code->ProcessAudio(&Thread, Engine);
    }
    // Win32_Window_RenderDebugAudio(Engine);
}

s32 APIENTRY _tWinMain(HINSTANCE instance, HINSTANCE prevInstance, LPTSTR cmdLine, int cmdShow)
{
    (void)instance;
    (void)prevInstance;
    (void)cmdLine;
    (void)cmdShow;

    // Executable info
    chk_win_executable_info ExeInfo = {0};
    Win32_Executable_Setup(&ExeInfo);

    // Test loading a file
    chk_debug_file BmpFile;
    Debug_LoadFile("test.bmp", &BmpFile);

    // Input
    chk_app_input Input = {0};

    // Window
    chk_win_app_window Window = {0};
    Window.Base.W = 1280;
    Window.Base.H = 720;
    Win32_Window_Setup(&Window, "chk_engine");
    Window.Base.Input = &Input;

    // Gamepad
    Window.Base.Input->Gamepads[0].Active = true;
    Win32_Gamepad_Setup(&Window.Base.Input->Gamepads[1]);

    // Config
    chk_app_config Config = {0};
    Config.CurrentGamepad = 0;
    Config.Frequency = 262;
    Config.Volume = 0.0125f;

    // Audio
    chk_win_app_audio_output Audio = {0};
    Audio.Base.NumberOfChannels = 2;
    Audio.Base.SamplesPerSecond = 48000;
    Audio.Base.BitsPerChannel = 16;
    Audio.Handle = Window.Handle;
    Win32_Audio_Setup(&Audio);
    Win32_Audio_FillBuffer(&Audio, Config.Frequency, Config.Volume, 0, Audio.Base.BufferSize);
    Win32_Audio_StartPlaying(&Audio);

    // Memory
    chk_win_app_memory Memory = {0};
    Memory.Base.MainSize = Megabytes(256);
    Memory.Base.TempSize = Gigabytes(1);
    Win32_Memory_Setup(&Memory);

    // DebugIO
    chk_debug_io DebugIO = {0};
    DebugIO.LoadFile = Debug_LoadFile;
    DebugIO.WriteFile = Debug_WriteFile;
    DebugIO.FreeFile = Debug_FreeFile;

    // Code
    chk_win_app_code Code = {0};
    Code.ExeInfo = &ExeInfo;
    const TCHAR AppCodePath[] = _T("libgame.dll");
    Win32_Executable_PathRelativeToExecutable(&ExeInfo, AppCodePath, ArrayCount(AppCodePath),
                                              Code.Filename, ArrayCount(Code.Filename));
    Win32_Code_Setup(&Code);

    // Engine
    chk_win_app_engine Engine = {0};
    Engine.Base.Window = &Window.Base;
    Engine.Base.Audio = &Audio.Base;
    Engine.Base.Memory = &Memory.Base;
    Engine.Base.Config = &Config;
    Engine.Base.Input = &Input;
    Engine.Code = &Code;

    // Debug Stuff
    chk_debug_logger DebugLogger = {0};
    DebugLogger.Log = CHKP_LogImp;
    DebugLogger.LogFormat = CHKP_LogFormatImp;
    
    chk_debug_checker DebugChecker = {0};
    DebugChecker.Check = CHKP_CheckImp;

    Engine.Base.m_DebugIO = &DebugIO;
    Engine.Base.m_DebugChecker = &DebugChecker;
    Engine.Base.m_DebugLogger = &DebugLogger;

    // Callbacks
    Win32_Window_SetOnDraw(&Window, OnDraw, &Engine);

    // Run the program
    Win32_Window_MainLoop(&Window);

    // Cleanup
    Win32_Code_Destroy(&Code);
    Win32_Memory_Destroy(&Memory);
    Win32_Window_Destroy(&Window);

    return (0);
}
