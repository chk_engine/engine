#pragma once

#include "../Common/DebugIO.h"
#include "../Common/Types.h"
#include "Win32.h"

CHK_DEBUG_LOAD_FILE(Debug_LoadFile);
CHK_DEBUG_WRITE_FILE(Debug_WriteFile);
CHK_DEBUG_FREE_FILE(Debug_FreeFile);
