#include "Gamepad.h"

b32 Win32_Gamepad_Setup(chk_gamepad_input* Gamepad)
{
    if (!Gamepad) { return (false); }

    return (true);
}

b32 Win32_Gamepad_Update(chk_gamepad_input* Gamepad)
{
    if (!Gamepad) { return (false); }

#define CHK_MACOS_UPDATE_GAMEPAD_BUTTON(Btn)                              \
    {                                                                     \
        Gamepad->Btn.Pressed = Gamepad->Btn.State && !Gamepad->Btn.Last;  \
        Gamepad->Btn.Released = !Gamepad->Btn.State && Gamepad->Btn.Last; \
        Gamepad->Btn.Last = Gamepad->Btn.State;                           \
    }

    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(LeftU)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(LeftL)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(LeftR)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(LeftD)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(RightU)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(RightL)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(RightR)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(RightD)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(Start)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(Select)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(Home)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(L1)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(L3)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(R1)
    CHK_MACOS_UPDATE_GAMEPAD_BUTTON(R3)

#undef CHK_MACOS_UPDATE_GAMEPAD_BUTTON

    return (true);
}

void Win32_Gamepad_UpdateAsKeyboard_Stick(chk_gamepad_axis* Axis, u16 State, b32 IsPositive)
{
    (void) State;
    (void) IsPositive;
    if (!Axis) { return; }

    // Check if the state is neutral or not
    /*
    if (Axis->State == 128)
    {
        Axis->State = (f32)((State) ? ((IsPositive) ? 255 : 0) : 128);
    } else {
        // If it's not neutral, go to neutral if reversed
        if (IsPositive && Axis->State < 128)
        {
            Axis->State = 128;
        } else if (!IsPositive && Axis->State > 128) {
            Axis->State = 128;
        } else
        {
            Axis->State = (f32)((State) ? ((IsPositive) ? 255 : 0) : 128);
        }
    }
    */
}

b32 Win32_Gamepad_UpdateAsKeyboard(chk_gamepad_input* Gamepad, u16 KeyCode, b32 State)
{
    if (!Gamepad) { return (false); }

    switch (KeyCode)
    {
        // DPad
        case VK_UP: Gamepad->LeftU.State = State; break;
        case VK_LEFT: Gamepad->LeftL.State = State; break;
        case VK_RIGHT: Gamepad->LeftR.State = State; break;
        case VK_DOWN: Gamepad->LeftD.State = State; break;

        // Buttons
        case 'I': Gamepad->RightU.State = State; break;
        case 'J': Gamepad->RightL.State = State; break;
        case 'L': Gamepad->RightR.State = State; break;
        case 'K': Gamepad->RightD.State = State; break;

        // L1/R1
        case 'U': Gamepad->L1.State = State; break;
        case 'O': Gamepad->R1.State = State; break;

        // Left Analog
        case 'W': Win32_Gamepad_UpdateAsKeyboard_Stick(&Gamepad->LStickY, (u16)State, false); break;
        case 'A': Win32_Gamepad_UpdateAsKeyboard_Stick(&Gamepad->LStickX, (u16)State, false); break;
        case 'D': Win32_Gamepad_UpdateAsKeyboard_Stick(&Gamepad->LStickX, (u16)State, true); break;
        case 'S': Win32_Gamepad_UpdateAsKeyboard_Stick(&Gamepad->LStickY, (u16)State, true); break;

        default: break;
    }

    return (true);
}
