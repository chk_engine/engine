#include "Path.h"

// String
b32 CatStrings(const TCHAR* StrA, umm CountA, const TCHAR* StrB, umm CountB, TCHAR* StrDst, umm CountDst)
{
    b32 CopiedAll = false;

    umm Index, Written = 0;
    for (Index = 0; Index < CountA && Written < CountDst; ++Index, ++Written) { *StrDst++ = *StrA++; }
    for (Index = 0; Index < CountB && Written < CountDst; ++Index, ++Written) { *StrDst++ = *StrB++; }
    if (Written == CountDst)
    {
        --StrDst;
        CopiedAll = false;
    }
    *StrDst++ = _T('\0');

    return (CopiedAll);
}

umm StringLength(const TCHAR* Str)
{
    umm Count = 0;

    while (*Str++ != _T('\0')) { ++Count; }

    return (Count);
}

b32 Win32_Executable_Setup(chk_win_executable_info* ExeInfo)
{
    if (!ExeInfo) { return (false); }
    ExeInfo->IsValid = true;

    // Get the path to the executable in the bundle
    u32 FileSize = ArrayCount(ExeInfo->Filename);
    if (GetModuleFileName(NULL, ExeInfo->Filename, FileSize) == 0)
    {
        ExeInfo->IsValid = false;
        return (false);
    }

    // Go back two folders in the path to find the Contents folder
    ExeInfo->LastSlash = ExeInfo->SecondLastSlash = ExeInfo->Filename;
    for (TCHAR* Scan = ExeInfo->Filename; *Scan != _T('\0'); Scan++)
    {
        if (*Scan == _T('/') || *Scan == _T('\\'))
        {
            ExeInfo->SecondLastSlash = ExeInfo->LastSlash;
            ExeInfo->LastSlash = Scan + 1;
        }
    }

    // Construct the path to Contents
    ExeInfo->FilenameCount = (ExeInfo->LastSlash - ExeInfo->Filename);
    ExeInfo->ContentsCount = (ExeInfo->SecondLastSlash - ExeInfo->Filename);

    const TCHAR DataPath[] = _T("Win\\Data\\");
    ExeInfo->DataCount = ExeInfo->ContentsCount + ArrayCount(DataPath) - 1;
    if (!Win32_Executable_PathRelativeToContents(ExeInfo, DataPath,
                                                 ArrayCount(DataPath), ExeInfo->Data,
                                                 ArrayCount(ExeInfo->Data)))
    {
        ExeInfo->IsValid = false;
        return (false);
    }

    return (true);
}

b32 Win32_Executable_PathRelativeToContents(chk_win_executable_info* ExeInfo, const TCHAR* Path, umm PathCount, TCHAR* Dst, umm DstCount)
{
    if (!ExeInfo || !ExeInfo->IsValid) { return (false); }
    CatStrings(ExeInfo->Filename, ExeInfo->ContentsCount, Path, PathCount, Dst, DstCount);
    return (true);
}

b32 Win32_Executable_PathRelativeToExecutable(chk_win_executable_info* ExeInfo, const TCHAR* Path, umm PathCount, TCHAR* Dst, umm DstCount)
{
    if (!ExeInfo || !ExeInfo->IsValid) { return (false); }
    CatStrings(ExeInfo->Filename, ExeInfo->FilenameCount, Path, PathCount, Dst, DstCount);
    return (true);
}

b32 Win32_Executable_PathRelativeToData(chk_win_executable_info* ExeInfo, const TCHAR* Path, umm PathCount, TCHAR* Dst, umm DstCount)
{
    if (!ExeInfo || !ExeInfo->IsValid) { return (false); }
    CatStrings(ExeInfo->Filename, ExeInfo->DataCount, Path, PathCount, Dst, DstCount);
    return (true);
}