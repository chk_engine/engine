#pragma once

#include "../Common/Memory.h"
#include "../Common/Types.h"

// Memory
typedef struct chk_win_app_memory
{
    chk_app_memory Base;
    b32 IsValid;
} chk_win_app_memory;

b32 Win32_Memory_Setup(chk_win_app_memory* Memory);
b32 Win32_Memory_Destroy(chk_win_app_memory* Memory);
