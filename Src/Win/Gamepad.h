#pragma once

#include "../Common/Gamepad.h"
#include "../Common/Types.h"
#include "Win32.h"

b32 Win32_Gamepad_Setup(chk_gamepad_input* Gamepad);
b32 Win32_Gamepad_Update(chk_gamepad_input* Gamepad);
b32 Win32_Gamepad_UpdateAsKeyboard(chk_gamepad_input* Gamepad, u16 KeyCode, b32 State);
