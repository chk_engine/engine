#include "Code.h"

// Stubs
CHK_PROCESS_FRAME(Stub_ProcessFrame)
{
    (void)Thread;
    (void)Engine;
}

CHK_PROCESS_AUDIO(Stub_ProcessAudio)
{
    (void)Thread;
    (void)Engine;
}

b32 Win32_CopyFileToTemp(const TCHAR* PathA, TCHAR* PathB)
{
    b32 Result = false;
    HANDLE FileA = INVALID_HANDLE_VALUE;
    HANDLE FileB = INVALID_HANDLE_VALUE;

    FileA = CreateFile(PathA, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (FileA == INVALID_HANDLE_VALUE) { LOGF_ERROR("Unable to open file '%ls'", PathA); goto _Win32_CopyFileToTemp_Done; }
    FileB = CreateFile(PathB, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (FileB == INVALID_HANDLE_VALUE) { LOGF_ERROR("Unable to open file '%ls'", PathB); goto _Win32_CopyFileToTemp_Done; }

    LARGE_INTEGER FileSize64;
    if (!GetFileSizeEx(FileA, &FileSize64))
    {
        LOG_ERROR("Failed to get the file size!");
        goto _Win32_CopyFileToTemp_Done;
    }

    s32 FileSize32 = (s32)FileSize64.QuadPart;
    u8 Buffer[4096];
    s32 BufferCount = ArrayCount(Buffer);

    DWORD Read, Written;
    while (FileSize32 > 0)
    {
        // Read Buffer or fewer bytes
        DWORD ToRead = (FileSize32 > BufferCount) ? BufferCount : FileSize32;
        if (ReadFile(FileA, Buffer, ToRead, &Read, NULL) == 0)
        {
            LOGF_ERROR("Error while reading from '%ls'", PathA);
            goto _Win32_CopyFileToTemp_Done;
        }

        // Write Read bytes
        if (WriteFile(FileB, Buffer, Read, &Written, NULL) == 0)
        {
            LOGF_ERROR("Error while writing to '%ls'", PathB);
            goto _Win32_CopyFileToTemp_Done;
        }

        FileSize32 -= (s32)Written;
    }

    Result = true;
_Win32_CopyFileToTemp_Done:
    if (FileB != INVALID_HANDLE_VALUE) { CloseHandle(FileB); }
    if (FileA != INVALID_HANDLE_VALUE) { CloseHandle(FileA); }
    return (Result);
}

b32 Win32_Code_Reset(chk_win_app_code* Code)
{
    if (!Code) { return (false); }

    Code->ProcessFrame = Stub_ProcessFrame;
    Code->ProcessAudio = Stub_ProcessAudio;
    Code->IsValid = false;

    return (true);
}

FILETIME Win32_Code_GetWriteTime(chk_win_app_code* Code)
{
    FILETIME WriteTime = {0};

    WIN32_FIND_DATA Data = {0};
    if (FindFirstFile(Code->Filename, &Data))
    {
        WriteTime = Data.ftLastWriteTime;
    }

    return (WriteTime);
}

b32 Win32_Code_Load(chk_win_app_code* Code)
{
    Win32_Code_Reset(Code);

    // Generate temporary path
    TCHAR TempDir[CHK_WIN32_MAX_FILENAME_SIZE];
    const TCHAR TempPath[] = _T("Temp\\");
    Win32_Executable_PathRelativeToExecutable(Code->ExeInfo, TempPath, ArrayCount(TempPath), TempDir, ArrayCount(TempDir));
    if (!GetTempFileName(TempDir, _T("chk_temp"), 0, Code->Tempname)) { return (false); }
    if (!Win32_CopyFileToTemp(Code->Filename, Code->Tempname))
    {
        LOG_WARNING("Failed to copy the DLL into the temporary!");
    }

    // Code->Library = dlopen(Code->Filename, RTLD_NOW);
    Code->Library = LoadLibrary(Code->Tempname);
    if (!Code->Library) { return (false); }

    Code->ProcessFrame = (chk_process_frame_func*)GetProcAddress(Code->Library, "ProcessFrame");
    if (!Code->ProcessFrame) { Code->ProcessFrame = Stub_ProcessFrame; }

    Code->ProcessAudio = (chk_process_audio_func*)GetProcAddress(Code->Library, "ProcessAudio");
    if (!Code->ProcessAudio) { Code->ProcessAudio = Stub_ProcessAudio; }

    Code->IsValid = (Code->ProcessFrame != Stub_ProcessFrame &&
                     Code->ProcessAudio != Stub_ProcessAudio);

    return (Code->IsValid);
}

b32 Win32_Code_Setup(chk_win_app_code* Code)
{
    if (!Code) { return (false); }

    Win32_Code_ReloadIfNewer(Code);

    if (!Code->IsValid) { Win32_Code_Reset(Code); }
    return (Code->IsValid);
}

b32 Win32_Code_Destroy(chk_win_app_code* Code)
{
    if (!Code) { return (false); }
    if (Code->Library) { FreeLibrary(Code->Library); }
    if (Code->Tempname[0]) { DeleteFile(Code->Tempname); }

    Code->Library = NULL;
    Win32_Code_Reset(Code);

    return (Win32_Code_Reset(Code));
}

b32 Win32_Code_ReloadIfNewer(chk_win_app_code* Code)
{
    if (!Code) { return (false); }

    FILETIME WriteTime = Win32_Code_GetWriteTime(Code);
    if (CompareFileTime(&WriteTime, &Code->LastWriteTime) > 0)
    {
        if (Code->LastWriteTime.dwLowDateTime == 0)
        {
            OutputDebugString(_T("Loading App code\n"));
        } else {
            OutputDebugString(_T("Reloading App code from a newer version!\n"));
        }

        Win32_Code_Destroy(Code);
        Win32_Code_Load(Code);
        Code->LastWriteTime = WriteTime;
    }

    return (true);
}
