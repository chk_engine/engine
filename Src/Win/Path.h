#pragma once

#include "../Common/Types.h"
#include "Win32.h"

#define CHK_WIN32_MAX_FILENAME_SIZE MAX_PATH

typedef struct chk_win_executable_info
{
    b32 IsValid;

    TCHAR Filename[CHK_WIN32_MAX_FILENAME_SIZE];
    TCHAR Data[CHK_WIN32_MAX_FILENAME_SIZE];

    umm FilenameCount;
    umm ContentsCount;
    umm DataCount;

    TCHAR* LastSlash;
    TCHAR* SecondLastSlash;
} chk_win_executable_info;

b32 Win32_Executable_Setup(chk_win_executable_info* BundleInfo);
b32 Win32_Executable_PathRelativeToContents(chk_win_executable_info* BundleInfo, const TCHAR* Path, umm PathCount, TCHAR* Dst, umm DstCount);
b32 Win32_Executable_PathRelativeToExecutable(chk_win_executable_info* BundleInfo, const TCHAR* Path, umm PathCount, TCHAR* Dst, umm DstCount);
b32 Win32_Executable_PathRelativeToData(chk_win_executable_info* BundleInfo, const TCHAR* Path, umm PathCount, TCHAR* Dst, umm DstCount);

// String
b32 CatStrings(const TCHAR* StrA, umm CountA, const TCHAR* StrB, umm CountB, TCHAR* StrDst, umm CountDst);
umm StringLength(const TCHAR* Str);
