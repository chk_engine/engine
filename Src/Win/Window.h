#pragma once

#include "../Common/Window.h"
#include "Win32.h"

typedef struct chk_win_app_window
{
    chk_app_window Base;

    // State
    b32 HasClosingDialog;

    // Win32
    HWND Handle;
    HACCEL Accelerators;
    WINDOWPLACEMENT Placement;
    OSVERSIONINFOEX VerInfo;
    DWORD SavedStyle, SavedStyleEx;
    BITMAPINFO BitmapInfo;
} chk_win_app_window;

// Base Operations
b32 Win32_Window_Setup(chk_win_app_window* Window, const char* Caption);
b32 Win32_Window_Destroy(chk_win_app_window* Window);
b32 Win32_Window_BeginFrame(chk_win_app_window* Window);
b32 Win32_Window_EndFrame(chk_win_app_window* Window);
b32 Win32_Window_PollEvents(chk_win_app_window* Window);
b32 Win32_Window_MainLoop(chk_win_app_window* Window);

// Operations
b32 Win32_Window_ToggleFullscreen(chk_win_app_window* Window);
b32 Win32_Window_SetFullscreen(chk_win_app_window* Window, b32 Value);
b32 Win32_Window_SetMinimized(chk_win_app_window* Window, b32 Value);
b32 Win32_Window_SetMaximized(chk_win_app_window* Window, b32 Value);

// Callbacks
b32 Win32_Window_SetOnSize(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr);
b32 Win32_Window_SetOnDraw(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr);
b32 Win32_Window_SetOnMove(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr);
b32 Win32_Window_SetOnQuit(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr);
b32 Win32_Window_SetOnFullscreen(chk_win_app_window* Window, chk_app_window_callback* Callback, void* UserPtr);
