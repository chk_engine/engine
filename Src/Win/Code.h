#pragma once

#include "../Common/Platform.h"
#include "../Common/Types.h"
#include "Io.h"
#include "Path.h"

typedef struct chk_win_app_code
{
    b32 IsValid;
    HMODULE Library;

    chk_process_frame_func* ProcessFrame;
    chk_process_audio_func* ProcessAudio;

    TCHAR Tempname[CHK_WIN32_MAX_FILENAME_SIZE];
    TCHAR Filename[CHK_WIN32_MAX_FILENAME_SIZE];
    FILETIME LastWriteTime;

    chk_win_executable_info* ExeInfo;
} chk_win_app_code;

b32 Win32_Code_Setup(chk_win_app_code* Code);
b32 Win32_Code_Destroy(chk_win_app_code* Code);
b32 Win32_Code_ReloadIfNewer(chk_win_app_code* Code);
