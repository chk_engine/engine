#pragma once

#include "../Common/Audio.h"
#include "../Common/Types.h"
#include "Win32.h"

#include <mmsystem.h>
#include <dsound.h>

#define CHK_DIRECT_SOUND_CREATE(Name) HRESULT WINAPI Name(LPCGUID pcGuidDevice, LPDIRECTSOUND *ppDS, LPUNKNOWN pUnkOuter)
typedef CHK_DIRECT_SOUND_CREATE(chk_direct_sound_create_func);

typedef struct chk_win_app_audio_output
{
    chk_app_audio_output Base;

    HWND Handle;
    HMODULE Module;
    LPDIRECTSOUND DSound;
    LPDIRECTSOUNDBUFFER PrimaryBuffer;
    LPDIRECTSOUNDBUFFER SecondaryBuffer;
} chk_win_app_audio_output;

b32 Win32_Audio_Setup(chk_win_app_audio_output* SoundOutput);
b32 Win32_Audio_StartPlaying(chk_win_app_audio_output* Audio);

void Win32_Audio_FillBuffer(chk_win_app_audio_output* Audio, u32 Frequency, f32 Volume, DWORD ByteToLock, DWORD BytesToWrite);
void Win32_Audio_WriteWave(chk_win_app_audio_output* Audio, u32 Frequency, f32 Volume);
