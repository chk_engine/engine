#include "DebugIO.h"
#include "Path.h"

CHK_DEBUG_LOAD_FILE(Debug_LoadFile)
{
    if (!FileOut) { return (false); }

    TCHAR TFilename[CHK_WIN32_MAX_FILENAME_SIZE];
    DWORD Written = MultiByteToWideChar(CP_UTF8, 0, Filename, -1, TFilename, ArrayCount(TFilename));
    if (Written == 0) { return (false); }

    chk_win_executable_info ExeInfo = {0};
    Win32_Executable_Setup(&ExeInfo);

    TCHAR AbsFilename[CHK_WIN32_MAX_FILENAME_SIZE] = {0};
    Win32_Executable_PathRelativeToData(&ExeInfo, TFilename, StringLength(TFilename), AbsFilename, ArrayCount(AbsFilename));

    HANDLE FileHandle = CreateFile(TFilename, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if (FileHandle == INVALID_HANDLE_VALUE) { return (false); }

    LARGE_INTEGER FileSizeLI;
    if (!GetFileSizeEx(FileHandle, &FileSizeLI))
    {
        CloseHandle(FileHandle);
        return (false);
    }
    u64 FileSize = FileSizeLI.QuadPart;

    FileOut->Memory = VirtualAlloc(0, FileSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    if (!FileOut->Memory)
    {
        CloseHandle(FileHandle);
        return (false);
    }

    DWORD BytesRead;
    if (!ReadFile(FileHandle, FileOut->Memory, (DWORD)FileSize, &BytesRead, 0) || FileSizeLI.QuadPart != BytesRead)
    {
        Debug_FreeFile(FileOut);
        CloseHandle(FileHandle);
        return (false);
    }
    CloseHandle(FileHandle);

    FileOut->MemorySize = FileSize;
    return (true);
}

CHK_DEBUG_WRITE_FILE(Debug_WriteFile)
{
    if (!FileIn) { return (false); }

    TCHAR TFilename[CHK_WIN32_MAX_FILENAME_SIZE];
    DWORD Written = MultiByteToWideChar(CP_UTF8, 0, Filename, -1, TFilename, ArrayCount(TFilename));
    if (Written == 0) { return (false); }

    chk_win_executable_info ExeInfo = {0};
    Win32_Executable_Setup(&ExeInfo);

    TCHAR AbsFilename[CHK_WIN32_MAX_FILENAME_SIZE] = {0};
    Win32_Executable_PathRelativeToData(&ExeInfo, TFilename, StringLength(TFilename), AbsFilename, ArrayCount(AbsFilename));

    HANDLE FileHandle = CreateFile(TFilename, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
    if (FileHandle == INVALID_HANDLE_VALUE) { return (false); }

    DWORD BytesWritten;
    if (!WriteFile(FileHandle, FileIn->Memory, (DWORD)FileIn->MemorySize, &BytesWritten, 0) || BytesWritten != FileIn->MemorySize)
    {
        CloseHandle(FileHandle);
        return (false);
    }
    CloseHandle(FileHandle);

    return (true);
}

CHK_DEBUG_FREE_FILE(Debug_FreeFile)
{
    if (FileIn && FileIn->Memory)
    {
        VirtualFree(FileIn->Memory, 0, MEM_RELEASE);
        FileIn->Memory = NULL;
        FileIn->MemorySize = 0;
        return (true);
    }
    return (false);
}

b32 DEBUG_Platform_DestroyFile(void* FileMemory)
{
    if (FileMemory)
    {
        VirtualFree(FileMemory, 0, MEM_RELEASE);
        return (true);
    }
    return (false);
}
