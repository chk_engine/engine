#pragma once

// Icons
#define IDI_ICON_MAIN 5

// Manifests
#define IDR_RT_MANIFEST_MAIN 1

// Accelerators
#define ACC_INGAME_ACTIONS 101
#define ACC_INGAME_TOGGLE_FULLSCREEN 40001
