#pragma once

#include "../Common/Types.h"

#define CINTERFACE
#define COBJMACROS
#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#define STRICT 1
#define NOMINMAX 1
#include <windows.h>

#include <tchar.h>
